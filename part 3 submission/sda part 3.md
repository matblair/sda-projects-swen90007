% SWEN90007 Software Design And Architecture - Project 3
% Software Architectural Design for Taut Messaging Feature
% Mathew Blair and James Fitzsimmons - Group 23

Overview
========

This document details the high-level architecture for our messaging application, Taut. It contains: high-level domain, architecture, and component diagrams; diagrams specifying how the various parts of the system interact; and our reasoning for choosing particular patterns. It extends upon the previous architecture document (hereafter known as V1) to support authentication and administration features.

Team Members
============

+ Mathew Blair - 541635
+ James Fitzsimmons - 382043


Use Cases
===================
Here we provide two use cases for the features implemented in the Taut prototype. The implemented features allow for user authentication, creating, reading editing and deleting messages, and for administrators to merge similar messages in the message stream to a single message, reducing clutter.

Use Cases for Messaging
=========================

Reading Messages
----------------

### Trigger

The user wishes to join a room to read messages.

### Preconditions

- The user is an employee at the company.
- The user is signed in.

### Basic Path

1. The user searches the list of rooms and attempts to join the room they're
   interested in.
2. The server verifies that the user has read access to the room.
3. The user granted access to the room.
4. The user is presented with the most recent messages in the room.

### Postconditions

None.

### Exception Paths

- At step 1, the search results will only show rooms that the user has
  visibility rights. If the room is not visible to the user, then the use case
  ends.
- At step 2, if the user doesn't have read access, then an error message is
  shown to the user and the use case ends.

Posting a Message
-----------------

### Trigger

The user wishes to post a message in a room.

### Preconditions

- The user is an employee at the company.
- The user is signed in.
- The user is in the room they wish to post in.

### Basic Path

1. The user types and submits their message.
2. The server verifies that the user has permission to post in the room.
3. Other members in the room are notified of the message.

### Postconditions

- The message is persisted in the room's message history.

### Exception Paths

- At step 2, if the user does not have permission to post in the room, then the
  message is not saved, and the use case ends.

Editing a Message
-----------------

### Trigger

The user wishes to edit a message they have sent.

### Preconditions

- The user is an employee at the company.
- The user is signed in.
- The user is in the room they wish to post in.
- The user has posted a message in the room.

### Basic Path

1. The user edits their message.
2. The server verifies that the message being edited belongs to the user, is the
   most recent message in the room, and that it has been less than one minute
   since the message was originally posted.
3. Other members in the room are notified of the changed message.

### Postconditions

- The room's message history is updated with the new message contents.

### Exception Paths

- At step 2, if any of the conditions fail, then the message is not edited, and
  the use case ends.

Deleting Messages
-----------------

### Trigger

The user wishes to delete a room.

### Preconditions

- The user is an employee at the company.
- The user is signed in.

### Basic Path

1. The user attempts to delete the room.
2. The server verifies that user has admin rights for the room being deleted.
3. The room is deleted.

### Postconditions

- The messages that had been posted in the room are deleted.

### Exception Paths

- At step 2, if the user doesn't have read access, then an error message is
  shown to the user and the use case ends.
- At step 3, if the server has been configured to not allow message deletion,
  then the room is archived (same appearance to end users but the data is kept)
  and the use case ends.


Use Cases for Authentication
=========================

Reading Messages
----------------

### Trigger

The user wishes to log in to Taut to use the application.

### Preconditions

- The user is an employee at the company.
- The user is not signed in.

### Basic Path

1. The user submits their user name and password
2. The user's username and password are validated
3. The user is returned a session token.

### Postconditions

- The User has been logged in and provided with a session token for continued access.

### Exception Paths

- At step 2, if the password does not match a 401 value is returned


Use Cases for Merging Messages
=========================

Reading Messages
----------------

### Trigger

A room admin wishes to merge multiple messages within a room into one message.

### Preconditions

- The room admin is an employee at the company.
- The room admin is the owner of that room 'n'.
- The room admin is signed in.

### Basic Path

1. The room admin retrieves all messages of the given room.
2. The room admin selects $n$ messages in that room that are similar.
3. The room admin requests the merge
4. The messages are merged in the room
	4a. The individual messages are soft-deleted in the room
	4b. The merged message containing all the text from the individual messages
		is placed in the room.

### Postconditions

- The previous messages are hidden from the room.
- The merged message is created in the room.

### Exception Paths

- At step 3, if the admin is not the owner of the room, then an error message
  is shown to the admin and the use case ends.
- At step 3, if any of the messages requested to be merged cannot be found in the database
  then an error message is shown to the admin and the use case ends.
- At step 3, If not all of the messages requested to be merged belong to the same room then
  an error message is shown to the admin and the user case ends.

Use Cases for Searching Messages
=========================

Searching By Keywords
----------------
### Trigger

A user wishes to search for messages containing some given keywords.

### Preconditions

- The user is an employee at the company.
- The user is signed in.

### Basic Path

1. The user accesses a room.
2. The user provides keywords to search by in that room.
3. If there are any messages that match the search request, then the
   messages are returned to the user.
4. If there are no message that match the search request, the user
   is shown an appropriate message.

### Postconditions

None.

### Exception Paths

- At step 1, if the user does not have access to the room, an error
  message is shown to the user and the use case ends.

High-Level Diagrams
===================
Here we provide the high level structural views of the Taut messaging application. These views demonstrate the relevant architectural components needed to satisfy the messaging use case, and do not represent the entire architecture of the application. This view has changed only slightly from the original document.

### Taut Domain
The domain of Taut, as shown in Figure \autoref{domain}, is that of a messaging system, and is independent of the particular implementation (that is a web based messaging system). Further, it represents the whole domain of this application, not just the messaging feature.

As with V1, there is still no concept of a `User` in the Taut domain. A `User` is a strictly Use-Case level entity, a way of restricting access to a resource based on some set of credentials. User accounts are also transient: a person may leave an organisation and therefore we may be required to remove their user account so that they no longer have access to the system. In these instances we will still want, for auditing purposes, to be able to track the actions and messages of certain individuals, of `People`. Whilst this distinction may not be necessary for a consumer focussed app (where we may not care about the person once they have left) it is prudent to include this distinction in our enterprise application for both accountability purposes and to allow us the flexibility to change how we handle User accounts without modifying the domain layer.

![Domain diagram for Taut. \label{domain}](domain_diagram.pdf "Domain diagram for Taut.")

## Architectural Diagram

At a the component level Taut follows the principles of The Clean Architecture, relying on dependency inversion to separate the more fragile and changeable aspects of the application from the constant or rarely modified components. Because of this there is a heavy reliance on interfaces. Further, these interfaces are often defined in the components that rely on them. For example, the `Person` component will define the `IPersonRepo`, that will be implemented in the `Repository` sub-component of the `Interface Adapter Layer`. These interfaces, along with where they are defined, are given in Figure \autoref{component}.

![Component diagram for Taut. \label{component}](component.pdf "Component diagram for Taut.")

### Interface Specifications
Here we provide a brief description as to the purpose of each interface, along with the methods on them. Note that this is not a complete implementation of the Taut system, and that these interfaces will be extended as more functionality is added.

### Infrastructure Layer Interfaces
#### IGoCraft
The `IGoCraft` interface provides the all of the basic web framework functionality required by our application. It is provided to us by the GoCraft/web framework. For details of the methods provided by this framework, please see the documentation for the framework [here](https://github.com/gocraft/web).

#### IGorilla
The `IGorllia` interface provides the all of session management facilities needed by our application. It is provided to us by the Gorilla/Sessions framework. For details of the methods provided by this framework, please see the documentation for the framework [here](http://www.gorillatoolkit.org/pkg/sessions).


#### IGORM
The `IGORM` interface provides the required functionality for the repositories to perform database actions. It is provided by the GORM ORM library, and relies on the definition of annotated types that are implemented in the Repositories component. For full details of this interface, please see the GORM documentation [here](https://github.com/jinzhu/gorm).

### Interface Adapter Layer Interfaces
#### ITautMessagingAPI
The `Taut Messaging API` is the public facing interface to the Taut Messaging system. This API is a restful JSON API, and is provided through resource oriented HTTP Requests. It provides the facility to create a message in a room and view a rooms messages, as well as delete and edit messages.

##### Methods
```
	// Return a list of the rooms available to the user.
	GET    "/api/rooms"

	// Delete a room.
	DELETE "/api/rooms/:id"

	// Gets the most recent messages in a room, up to some limit.
	GET    "/api/rooms/:room_id/messages?limit=10"

	// Post a new message in a room.
	POST   "/api/rooms/:room_id/messages"

	// Edit an existing message.
	POST   "/api/messages/:id"

	// Log in to the system
	POST   "/api/sessions/login"

	// Log out of the system
	DELETE "/api/sessions/logout"

	// Search for messages in a room
	GET    "/api/rooms/:id/search?keywords=comma,separated,key,words

	// Merge messages in a room
	POST   "/api/rooms/:id/messages/merge"
```

#### IUserRepo
The `IUserRepo`, implemented in the Interface Adapter Layer, is defined in the User component of the Use Case Layer and specifies the required methods to retrieve and store objects in the Person repository.

##### Methods
```
	FindById(id : int) : (User, error)
```

#### IPersonRepo
The `IPersonRepo`, implemented in the Interface Adapter Layer, is defined in the Person component of the Domain Layer and specifies the required methods to retrieve and store objects in the Person repository.

##### Methods
```
	FindById(id : int) : (Person, error)
```

#### IMessageRepo
The `IMessageRepo`, implemented in the Interface Adapter Layer, is defined in the Message component of the Domain Layer and specifies the required methods to retrieve and store objects in the Message repository.

##### Methods
```
	FindById(id : int) (Message, error)
	FindByIds(ids : int[]) : (Message[], error)
	FindByRoom(room : Room, limit : int) : (Message[], error)
	FindByKeywords(room: Room, keywords: string[]) : (Message[], error)
	Store(message : Message)
	Update(message : Message)
	Delete(message : Message)
	DeleteAll(messages: Message[])
```

#### IRoomRepo
The `IRoomRepo`, implemented in the Interface Adapter Layer, is defined in the Room component of the Domain Layer and specifies the required methods to retrieve and store objects in the Room repository.

##### Methods
```
	All() : ([]Room, error)
	FindById(id : int) : (room: Room, error : Error)
	Delete(room : Room)
```

#### IMessageContext
This private interface defines the relevant methods to handle all requests dealing with Messages in Taut. It is defined and implemented in the MessageContext component.

##### Methods
```
	MessageEdit(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	MessageDelete(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
```

#### IRoomContext
This private interface defines the relevant methods to handle all requests dealing with Rooms in Taut. It is defined and implemented in the RoomContext component.

##### Methods
```
	RoomsList(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomMessagesList(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomMessagePost(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomDelete(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomMergeMessages(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
```

#### IAuthenticationContext
This private interface defines the relevant methods to handle all requests dealing with user authentication in Taut. It is defined and implemented in the AuthenticationContext component.

##### Methods
```
	AuthLogin(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	AuthLogout(c : *ApiContext, rw  : web.ResponseWriter, req : *web.Request)
```

### Use Case Layer Interfaces
#### IRoomInteractor
The IRoomInteractor interface provides the input and output interfaces for all Room manipulation Use Cases. It provides these interfaces through function definitions, that are implemented in the Room Interactor component.

##### Methods
```
	ListAll(presenter : IRoomPresenter, userId : int)
	AddMessage(presenter : IRoomPresenter, userId : int, roomId : int, content : string)
	LoadMessages(presenter : IRoomPresenter, userId : int, roomId : int, limit : int)
	Delete(presenter : IRoomPresenter, userId : int, roomId : int)
	MergeMessages(presenter : IRoomPresenter, userId : int, roomId : int, messageIds : int[])
	SearchMessages(presenter : IRoomPresenter, userId : int, roomId : int, keywords : string[])
```

#### IRoomPresenter
The IRoomPresenter interface provides the output formats for Room manipulation use cases.

#### Methods
```
	PresentListAll(rooms : Room[], error : Error)
	PresentAddMessage(message : Message, error : Error)
	PresentLoadMessages(messages : Message[], error : Error)
	PresentDelete(error : Error)
	PresentMergeMessages(message : Message, error : Error)
	PresentSearchMessages(messages : Messages[], error : Error)
```

#### IMessageInteractor
The IMessageInteractor interface provides the input and output interfaces for all Room manipulation Use Cases. It provides these interfaces through function definitions, that are implemented in the Message Interactor component.

##### Methods
```
	Edit(presenter : IMessagePresenter, userId : int, messageId : int, newContent : string)
```

#### IMessagePresenter
The IMessagePresenter interface provides the output formats for Message manipulation use cases.

##### Methods
```
	PresentEdit(message : Message, error : Error)
```

#### IUserInteractor
The IUserInteractor interface provides the input and output interfaces for all User manipulation Use Cases. It provides these interfaces through function definitions, that are implemented in the User Interactor component.

##### Methods
```
	ValidateCredentials(presenter : IUserPresenter, username : string, password : string)
```

#### IUserPresenter
The IUserPresenter interface provides the output formats for User manipulation use cases.

##### Methods
```
	PresentValidateCredentials(user : User, error : Error)
```

#### IUser
The private IUser interface exports the User use-case entity data type and associated interfaces to the Use Case layer for use within use cases. It may, in future, be exported publicly to the Interface Adapter Layer for use in filters in the `TautFrontController`.

### Domain Layer Interfaces
#### IDomain
The `IDomain` interface exports the data types and functions available on the Domain models to the Use Case layer for use in implementing the Use Case, as well as the defining the associated interfaces.


Structural Views
================
Here we show a class diagram of the implementation and specification of the interfaces within Taut. This is shown on a per component basis for clarity. Given these interfaces are well specified, it is entirely appropriate to work on components in isolation to ensure clarity. The interfaces are as specified above, and the domain entities are specified in the domain model. Here we present only the views which have changed in order to implement the new authentication features and message merging.

![Class diagram for Taut’s Interface Adapter Component.](interface_adapters.pdf "Class diagram for Taut’s Interface Adapter Web Services and Use Case Components.")

![Class diagram for Taut’s Interface Adapter Repositories Component.](interface_adapters_repos.pdf "Class diagram for Taut’s Interface Adapter Repositories Component.")

Behavioural Views
=================
These views demonstrate the behaviour of the system by demonstrating the sequence of events required to handle any and all relevant incoming requests. They make use of the interfaces defined earlier in the Interface Specifications to accomplish this.

Interaction Diagrams
--------------------
Here we specify the individual interactions required to satisfy these use cases. Where possible, interactions are defined earlier and reused in later components to clarify the documentation and reflect the desire for non-duplicated implementation.

### General Interactions
These interactions demonstrate the reusable components of future sequence diagrams, such as retrieving a `Room` using the `RoomRepository`. They also define the sequence diagrams for handling the incoming requests and the request lifecycle, presented in generic terms.

#### Handling Requests
Here in \autoref{apply-filters} we present an addition to the method by which the contexts are retrieved and initialised in an incoming request. Because of the nature of GoLang, some of the interactions here are hard to represent (due to heavy use of anonymous functions) and as such have been reduced to their equivalent in `setUserID` and `setInteractors`.

In practice, this setup is handled by the GoCraft/web library. We present it here to provide an understanding as to how requests are being handled and provide insight as to where contexts come from. This diagram will be used in demonstrating how each individual request type works by retrieving the context. We also include our authentication protocol, which is an addition to the original specification provided in V1.

![Request Handling - Creating Filters \label{request-handling}](request handling.pdf "Request Handling.")

![Request Handling - Applying filters \label{apply-filters}](apply_filters.pdf "Filter Application.")

#### Authenticating A User
The interaction diagram in \autoref{interaction-user-login} demonstrates the sequence of steps invoked when logging in as a user. This is performed in the context of the request handling in \autoref{request-handling}

![Interaction diagram for Taut’s user login use case. \label{interaction-user-login}](user_login.pdf)

#### Logging out as A User
The interaction diagram in \autoref{interaction-user-logout} demonstrates the sequence of steps invoked when logging out as a user. This is performed in the context of the request handling in \autoref{request-handling}

![Interaction diagram for Taut’s user login out case. \label{interaction-user-logout}](user_logout.pdf)

#### Searching for Messages in A Room
The interaction diagram in \autoref{interaction-room-search} demonstrates the behaviour required to search all messages within a room for a given set of keywords.

![Interaction diagram for Taut’s room message search use case. \label{interaction-room-search}](room_search.pdf)

### Merging Messages in A Room
The interaction diagram in \autoref{interaction-room-merge} demonstrates the behaviour required to merge multiple messages within a room by a room admin. 

![Interaction diagram for Taut’s room message merging use case. \label{interaction-room-merge}](merge_messages.pdf)

Architectural Patterns
======================

Our overall architectural pattern remains unchanged from V1: we have simply added new components and functionality to existing components as required. We feel we have justified the value of this architecture and its flexibility in dealing with these changes in the V1 specification and so will be omitting the description here as requested.

Design Patterns
===============

All of the design patterns that were used for the first feature were also used for the second feature, and so the justifications have been reproduced below. The only exceptions are where the original justifications referenced our planned second feature. Those references have been removed.

The concurrency section also changed slightly, in that it now discusses how multiple requests can be handled at the same time.

In order to have the endpoints be authenticated, session management was introduced. A section covering the relevant design pattern has been added at the bottom.

Common Patterns
---------------

Throughout the application, Taut makes heavy use of the dependency injection pattern. Indeed, dependency injection is fundamental to the clean architecture pattern.

The dependency injection pattern allows each layer to specify functionality that it depends upon, without concerning itself with the actual implementation, by way of interfaces.

Not only does this provide a good separation of concerns, it facilitates proper unit testing, as classes can be easily tested with mocked dependencies. For large scale enterprise applications, adequate testability is an absolute must to ensure code is behaving as expected, and regressions are not introduced.

Data Persistence
----------------

For data persistence, Taut uses the repository pattern, with the domain layer specifying interfaces for each of the entities’ repositories. This provides an abstraction between the domain layer and the underlying storage technologies.

Underpinning the repository implementation is GORM, an object relational mapper for GoLang (as opposed to the Grails ORM with the same name). GORM handles mapping in-memory entity objects into rows in the database, and vice versa.

Although introducing a data mapper can add complexity to an application (by way of increased code), by delegating this to an existing library, the costs have been minimised, while maintaining the benefits of loose coupling between the domain and data storage layers.

A consequence of using the clean architecture pattern is that domain entities are unaware of implementation details of the data persistence. The nature of GORM is that is requires annotations on structs in order to set up relationships between entities. As such, Taut’s repository implementations handle translating between the database entities and the domain entities.

Wrapping a data mapper inside the repository pattern allows Taut’s domain layer to remain independent of persistence mechanism. If desired, the particular ORM library being used could be substituted, or even replaced with another mechanism such as a web API, all without changes to code in the domain layer.

Object-to-Relational Structure
------------------------------

GORM uses a number of design patterns to translate database rows into the in-memory entities.

As is very often the case when dealing with this problem, GORM uses the identity field pattern to track which struct corresponds to which database row. GORM can be configured to use meaningful or meaningless keys, but in Taut, only meaningless keys are used. The keys are all simple and table-unique, and are generated automatically by the database.

Relationships between entities are handled using both the foreign key mapping and association table mapping patterns. As GORM relies on the database to generate new keys for objects, sometimes inserting objects with relations requires two queries (so that the foreign key is known for the dependent object), however GORM handles this automatically.

One of the downsides to foreign key mapping is that it introduces coupling between the domain layer and the data persistence layer, as the domain entities must be aware of how the data layer tracks relationships. With the relationship pattern and its additional translation between domain entity structs and database entity structs, this downside is completely negated.

Object-to-Relational Behaviour
------------------------------

The identity map pattern could easily be used for Taut’s messaging feature, to cache recently posted messages in memory. One limitation with GORM though is that it doesn’t provide any facilities for caching objects. Should performance prove to be an issue, the relevant repositories’ implementations could easily be extended to make use of this pattern.

At this stage, Taut is not making use of the unit of work pattern. With the server being a stateless web API, each endpoint has been designed to perform a single focused task. Message posting can be handled by a single insert query, with updates, loads, and deletion also each only requiring a single query.

Concurrency
-----------

As was the case with the first feature, the common problems brought by introducing concurrency are not present. The design however does allow for the addition of locking: the repository layer could provide methods to obtain and release locks on entities, which the interactors could then make use of.

Go's net/http library provides concurrency support out-of-the-box. Each request that comes into the server is handled by a goroutine, which is similar to a thread (although not the same). The request is given a unique context struct, which contains references to the (shared) interactors, but also unique information such as the session and current user (if logged in).

Presentation
------------

GoCraft/web is the web library on which Taut’s API layer is built. GoCraft/web provides a light wrapper around GoLang’s net/http library, uses the front controller design pattern for delegating request handling. Such a pattern allows for simple configuration, as all API requests can be passed through the same set of filters before being handed off to specialised functions.

In the clean architecture, the use case layer is responsible for determining what data needs to be shown. It then calls a presenter to show that data, with the presenter being defined in the interface adapter layer. In Taut, the presenter implementations transform the domain objects into JSON DAOs using the json-binding package.

Sessions and Authentication
---------------------------

For authentication, Taut uses the server session state design pattern, with cookies being used to identify the client. In particular, Taut uses the Gorilla/sessions package because of its data storage abstractions.

The session is used to track the User ID of the client, if they're logged in. This is done using the authentication enforcer pattern: every API endpoint has an authentication filter applied, and if the user is not logged in, the server returns the appropriate status code.

Session information is stored on the server in order to prevent the user from forging their ID and pretending to be someone else. Due to the very limited amount of data being stored in each session, the session information is all cached in memory, providing very high performance. If the Taut server needed to be scaled out horizontally to support more users, Gorilla/sessions allows for easily switching to the database session state pattern, as it can be extended to have any type of backing data store.

Although having a notion of sessions is not strictly speaking "RESTful", we opted to go this route because of the complexity involved in setting up something else like OAuth, particularly in Go. Sessions on the other hand are very well supported. That being said, in order to abstract this information away from the ApiContext struct, a filter is used to extract the session information and provide the ID of the currently logged-in user to the context. As such, if authentication was changed from a session-based solution to something more suitable to RESTful APIs, none of the context methods would have to change, only the authentication filter.

References
==========

1. Novak J. gocraft/web. 2015. Available at: https://github.com/gocraft/web. Accessed September 8, 2015.
2. Martin R. The Clean Architecture | 8th Light. Blog8thlightcom. 2012. Available at: http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html. Accessed September 10, 2015.
3. Kiessling M. Applying The Clean Architecture to Go applications » The Log Book of Manuel Kiessling. Manuelkiesslingnet. 2012. Available at: http://manuel.kiessling.net/2012/09/28/applying-the-clean-architecture-to-go-applications/. Accessed September 10, 2015.
