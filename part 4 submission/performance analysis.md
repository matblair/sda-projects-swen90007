% SWEN90007 Software Design And Architecture - Project 3
% Performance Analysis and Discussion for Taut - A Messaging Service
% Mathew Blair and James Fitzsimmons - Group 23

Overview
========

This document details the performance considerations made in building Taut, a messaging system prototype written in Golang. It also details the results of end to end performance tests, created using JMeter and tested over a local connection to a web server running an Intel i7 3470 QM processor with 16GB ram to give an overview of the expected performance of the system prototype.

Team Members
============

+ Mathew Blair - 541635
+ James Fitzsimmons - 382043


Performance Results
===================
Here we present the results from our series of JMeter tests investigating the performance of the web server. To simulate real use, we have a number of requests occurring simultaneously where users:

- Log in with valid credentials.
- View all rooms.
- View messages within a room.
- Log out
- Log in with invalid credentials.

This simulates the majority of use cases within the system, in the rough proportions at which they will occur. We run this sequence for 100, 200, 300 and 500 concurrent users, outputting the maximum, minimum, median, and mean times along with the percentages of errors encountered. Each user runs the above behavior for 10 iterations. We then review the throughput and error rates to determine the maximum number of concurrent users and concurrent requests the system can support.

All tests were run over a local network on an Intel i7 3470 running at 2.7 GHz, with 16GB of ram over a WIFI 802.11N connection. The Apache JMeter test file can be found as an XML appendix to this report for confirmation and repeatability of the tests. It is important to note that all the response time figures show average response time over the whole test, and the tests were set to ramp up additional users as time progresses, indicating the initial speedup and eventual slow down, with all users being active over the middle portion of the tests.

For 100 users we found reasonable performance characteristics across all routes, as shown here in \autoref{aggregate 100}:

![Aggregate Performance for 100 Users Running 10 Times. \label{aggregate 100}](aggeragate 100.png)

Login is our worst performing end point, which is understandable given the computational load required. Each time Login is called a bycrpt function is run to compare the hashed password stored in the database with the provided password. This is by far the most computationally expensive behavior in our application. We have minimized this in our invalid login test however, as this test uses an invalid user, and we only test password correctness with a valid username. Therefore, if you were to try to login with a valid username but invalid password the performance would be equivalent to the valid login performance.

The response times over the course of the test do remain consistent however, as shown in \autoref{response time 100}.

![Response Time for 100 Users Running 10 Times. \label{response time 100}](response time 100.png)

This performance yields a reasonably respectable throughput of 275.1 requests per second with no errors (we expect 100% error for invalid login). When we start to increase the number of concurrent users beyond this however, we see a notable increase in response time and also error rate. When increasing our number of simultaneous users we see our average response time for Login peak to 1600 ms, with the maximum response time at 3600 ms. This is a significant doubling of response time and not ideal for an enterprise web application. The response times for all other services double on average as well, as shown in \autoref{aggregate 200} and \autoref{response time 200}. Our throughput improved at 294.7 requests per second with the increase in users, indicating that we were not at our maximum capacity at 100 users.

![Aggregate Performance for 200 Users Running 10 Times. \label{aggregate 200}](aggeragate 200.png)

![Response Time for 200 Users Running 10 Times. \label{response time 200}](response time 200.png)


Increasing the number of concurrent users to 300 really demonstrates inadequacy of our system as we begin to see the error rate creep up as we have request time outs. View the system logs for these tests show that a large number of the errors stem from inadequacies in SQLite3 as a database back end for this many concurrent connections, as there were continual database timeouts. \autoref{aggregate 300} and \autoref{response time 300} show the performance characteristics for the system under this degree of load. Once again our throughput increased to 313.7 requests per second with 300 users.

![Aggregate Performance for 300 Users Running 10 Times. \label{aggregate 300}](aggeragate 300.png)

There is a notable response time spike in the 300 user test for the login function, which we initially put down to a fluke but on rerunning the test suite multiple times we still had this consistent spike. Our theory for this is that this is at the threshold for concurrent SQLite connections and that the framework tries to maintain the connections and not error until it gives up and drops the connections, then continuing normally but with a higher error rate.

![Response Time for 300 Users Running 10 Times. \label{response time 300}](response time 300.png)

Finally, running with 500 concurrent users showed similar performance to the 300 user metric, however our error rate crept up again to 16% for a number of database retrieval calls. This indicates that this is probably our maximum response time average, and that further tests will simply increase this error rate. This test had the highest throughput of 335.6 requests per second of all tests.

![Aggregate Performance for 500 Users Running 10 Times. \label{aggregate 500}](aggeragate 500.png)

![Response Time for 500 Users Running 10 Times. \label{response time 500}](response time 500.png)

As shown, there is still quite a lot of optimization, primarily in the case of database management (as discussed below in Architectural Features) that would need to be improved before deploying our application for enterprise use.

Performance Principles
=======================
In implementing our prototype system, performance was not our main concern. We instead focused on the functional implementation of the requirements in a functional API endpoint. That being said, we did consider performance at the design level, both in the choice of language and choice of application architecture.

Choice Of Language
-------------------
Our first choice made was the us of the Go programming language (golang). Go offers a rather unique and native support for concurrent programming which is not offered in a majority of languages, which makes developing performant applications on modern server hardware significantly easier and cheaper. Further to this, Go can be cross compiled to native machine code, and therefore increases the scope of possible deployment scenarios for our application.

Architectural Features
----------------------
In designing the architecture of Taut we have, by adopting the Clean Architecture[2] approach, isolated the components that deal with data persistence and caching from the use case and domain layers. This approach was chosen as it allows us to separate the components of the program that are likely to change (database implementations, routing engines etc.) from the core business requirements and use cases. In considering performance, this approach allows us to engineer the performance attributes and easily tweek these implementation details without impacting the core business logic.

Given our application domain, the majority of requests are in fact, independent and do not have any dependencies on any external applications. Instead our application acts as both a source of data and a sink of data, dealing with requests to add or change data or to retrieve data. Whether this is through the API we have provided or through integrations with existing systems, this means that we are primarily concerned with two performance attributes, the throughput of requests and the speed at which we can read and write data.

To handle throughput, the choice of Go provides inbuilt language support for concurrent request handling. The framework chosen, GoCraft[1] sits atop the core net/http framework which provides an automated parallelized routing platform that uses GoRoutines to dispatch a thread per request and then uses Go Channels to return that data when complete to relay to the user. This is an application of a system pipelining approach, where we dispatch request handlers and continue to server the next request while we wait for the result. As expected, this reduces our reliance on decreasing system latency, and means that simple requests are not penalized by being forced to wait for more complex requests. The usual downsides of this approach, complexity and dependencies do not apply as the complexity is handled by the language itself and the relevance of the pipeline being full is irrelevant given the performance overhead of spawning a goroutine is so minimal.

Each GoRoutine is then itself, through the use of the dependency injection for presentation to a use case handler, able to spawn more GoRoutines if needed and then write the result to the presenter through a Message Channel. This process can be repeated ad infinitum when required to increase program throughput.

Our second issue, the speed of accessing and writing data, is kept isolated from the rest of the application through the use of the Repository interfaces. Recall our Domain layer defines a series of interfaces, IUserRepository, IMessageRepository etc. that are then implemented in the Interface Adapter Layer. The actual storage of data is opaque to the Domain and Use Case layer, as long as the storage mechanism conforms the required interface. In our prototype, we make use of GORM as the physical storage provider, which provides an ORM wrapper around a SQLite3 database. As a prototype, this is functional and suits our needs but is not at all performant enough for a real world system.

To change this, we would propose moving to either a more performant database system (Postgres or MySQL) interfaced through GORM, which would provide instant performance improvements, and then add a layer of caching on top. Our system demonstrates characteristics that are ideal targets for improving performance through caching. We have guaranteed spacial locality, users access certain rooms are more likely to access messages from that room again. Likewise, users from certain departments are likely to access messages from rooms related to that department. Finally, given the real time nature of the application, users are more likely to be interested in new messages than old messages, so we can exploit this behavior in increasing the memory read speed of newer messages and moving older messages to slower less performant database storage systems.

We can also consider the performance as the system scales. As more users join the system (particularly if offered as a Software as a Service Platform) the database space will grow and the indexes will need to increase. Being able to shard or replicate these databases can become key to improving performance of the system, particularly if you have a business with multiple locations, where there is considerable use both within the building and between buildings. Our architecture easily allows for us to implement an eventually consistent replication system at the interface layer, without needing to modify or add to the use case or domain layer, making scaling to this level very straightforward.

References
==========

1. Novak J. gocraft/web. 2015. Available at: https://github.com/gocraft/web. Accessed September 8, 2015.
2. Martin R. The Clean Architecture | 8th Light. Blog8thlightcom. 2012. Available at: http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html. Accessed September 10, 2015.


Appendices
==========
JMeter Test File
----------------
```xml
<?xml version="1.0" encoding="UTF-8"?>
<jmeterTestPlan version="1.2" properties="2.8" jmeter="2.13 r1665067">
  <hashTree>
    <TestPlan guiclass="TestPlanGui" testclass="TestPlan" 
    testname="build-web-test-plan" enabled="true">
      <stringProp name="TestPlan.comments"></stringProp>
      <boolProp name="TestPlan.functional_mode">false</boolProp>
      <boolProp name="TestPlan.serialize_threadgroups">false</boolProp>
      <elementProp name="TestPlan.user_defined_variables" elementType="Arguments"
       guiclass="ArgumentsPanel" testclass="Arguments" 
       testname="User Defined Variables" enabled="true">
        <collectionProp name="Arguments.arguments"/>
      </elementProp>
      <stringProp name="TestPlan.user_define_classpath"></stringProp>
    </TestPlan>
    <hashTree>
      <ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" 
      testname="JMeter Users" enabled="true">
        <stringProp name="ThreadGroup.on_sample_error">continue</stringProp>
        <elementProp name="ThreadGroup.main_controller" elementType="LoopController"
         guiclass="LoopControlPanel" testclass="LoopController" 
         testname="Loop Controller" enabled="true">
          <boolProp name="LoopController.continue_forever">false</boolProp>
          <stringProp name="LoopController.loops">10</stringProp>
        </elementProp>
        <stringProp name="ThreadGroup.num_threads">300</stringProp>
        <stringProp name="ThreadGroup.ramp_time">5</stringProp>
        <longProp name="ThreadGroup.start_time">1373789594000</longProp>
        <longProp name="ThreadGroup.end_time">1373789594000</longProp>
        <boolProp name="ThreadGroup.scheduler">false</boolProp>
        <stringProp name="ThreadGroup.duration"></stringProp>
        <stringProp name="ThreadGroup.delay"></stringProp>
      </ThreadGroup>
      <hashTree>
        <OnceOnlyController guiclass="OnceOnlyControllerGui" testclass="OnceOnlyController"
         testname="Once Only Controller" enabled="false"/>
        <hashTree>
          <HTTPSamplerProxy guiclass="HttpTestSampleGui" 
          testclass="HTTPSamplerProxy" testname="Login Valid" enabled="true">
            <elementProp name="HTTPsampler.Arguments" elementType="Arguments"
             guiclass="HTTPArgumentsPanel" testclass="Arguments" 
             testname="Variables pré-définies" enabled="true">
              <collectionProp name="Arguments.arguments">
                <elementProp name="username" elementType="HTTPArgument">
                  <boolProp name="HTTPArgument.always_encode">true</boolProp>
                  <stringProp name="Argument.value">jrfitzsimmons</stringProp>
                  <stringProp name="Argument.metadata">=</stringProp>
                  <boolProp name="HTTPArgument.use_equals">true</boolProp>
                  <stringProp name="Argument.name">username</stringProp>
                </elementProp>
                <elementProp name="password" elementType="HTTPArgument">
                  <boolProp name="HTTPArgument.always_encode">true</boolProp>
                  <stringProp name="Argument.value">password</stringProp>
                  <stringProp name="Argument.metadata">=</stringProp>
                  <boolProp name="HTTPArgument.use_equals">true</boolProp>
                  <stringProp name="Argument.name">password</stringProp>
                </elementProp>
              </collectionProp>
            </elementProp>
            <stringProp name="HTTPSampler.domain"></stringProp>
            <stringProp name="HTTPSampler.port"></stringProp>
            <stringProp name="HTTPSampler.connect_timeout"></stringProp>
            <stringProp name="HTTPSampler.response_timeout"></stringProp>
            <stringProp name="HTTPSampler.protocol"></stringProp>
            <stringProp name="HTTPSampler.contentEncoding"></stringProp>
            <stringProp name="HTTPSampler.path">/auth/login</stringProp>
            <stringProp name="HTTPSampler.method">POST</stringProp>
            <boolProp name="HTTPSampler.follow_redirects">false</boolProp>
            <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
            <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
            <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
            <boolProp name="HTTPSampler.monitor">false</boolProp>
            <stringProp name="HTTPSampler.embedded_url_re"></stringProp>
          </HTTPSamplerProxy>
          <hashTree>
            <ResultCollector guiclass="StatVisualizer" testclass="ResultCollector" 
            testname="Aggregate Report" enabled="true">
              <boolProp name="ResultCollector.error_logging">false</boolProp>
              <objProp>
                <name>saveConfig</name>
                <value class="SampleSaveConfiguration">
                  <time>true</time>
                  <latency>true</latency>
                  <timestamp>true</timestamp>
                  <success>true</success>
                  <label>true</label>
                  <code>true</code>
                  <message>true</message>
                  <threadName>true</threadName>
                  <dataType>true</dataType>
                  <encoding>false</encoding>
                  <assertions>true</assertions>
                  <subresults>true</subresults>
                  <responseData>false</responseData>
                  <samplerData>false</samplerData>
                  <xml>false</xml>
                  <fieldNames>false</fieldNames>
                  <responseHeaders>false</responseHeaders>
                  <requestHeaders>false</requestHeaders>
                  <responseDataOnError>false</responseDataOnError>
                  <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
                  <assertionsResultsToSave>0</assertionsResultsToSave>
                  <bytes>true</bytes>
                  <threadCounts>true</threadCounts>
                </value>
              </objProp>
              <stringProp name="filename"></stringProp>
            </ResultCollector>
            <hashTree/>
            <Summariser guiclass="SummariserGui" testclass="Summariser" 
            testname="Generate Summary Results" enabled="true"/>
            <hashTree/>
            <ResultCollector guiclass="GraphVisualizer" testclass="ResultCollector"
             testname="Graph Results" enabled="true">
              <boolProp name="ResultCollector.error_logging">false</boolProp>
              <objProp>
                <name>saveConfig</name>
                <value class="SampleSaveConfiguration">
                  <time>true</time>
                  <latency>true</latency>
                  <timestamp>true</timestamp>
                  <success>true</success>
                  <label>true</label>
                  <code>true</code>
                  <message>true</message>
                  <threadName>true</threadName>
                  <dataType>true</dataType>
                  <encoding>false</encoding>
                  <assertions>true</assertions>
                  <subresults>true</subresults>
                  <responseData>false</responseData>
                  <samplerData>false</samplerData>
                  <xml>false</xml>
                  <fieldNames>false</fieldNames>
                  <responseHeaders>false</responseHeaders>
                  <requestHeaders>false</requestHeaders>
                  <responseDataOnError>false</responseDataOnError>
                  <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
                  <assertionsResultsToSave>0</assertionsResultsToSave>
                  <bytes>true</bytes>
                  <threadCounts>true</threadCounts>
                </value>
              </objProp>
              <stringProp name="filename"></stringProp>
            </ResultCollector>
            <hashTree/>
          </hashTree>
        </hashTree>
        <ConfigTestElement guiclass="HttpDefaultsGui" testclass="ConfigTestElement"
         testname="HTTP Request Defaults" enabled="true">
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" 
          guiclass="HTTPArgumentsPanel" testclass="Arguments" 
          testname="User Defined Variables" enabled="true">
            <collectionProp name="Arguments.arguments">
              <elementProp name="" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value"></stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.domain">10.9.234.169</stringProp>
          <stringProp name="HTTPSampler.port">12000</stringProp>
          <stringProp name="HTTPSampler.connect_timeout"></stringProp>
          <stringProp name="HTTPSampler.response_timeout"></stringProp>
          <stringProp name="HTTPSampler.protocol"></stringProp>
          <stringProp name="HTTPSampler.contentEncoding"></stringProp>
          <stringProp name="HTTPSampler.path"></stringProp>
          <stringProp name="HTTPSampler.implementation">HttpClient4</stringProp>
          <stringProp name="HTTPSampler.concurrentPool">4</stringProp>
        </ConfigTestElement>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy"
         testname="Login Valid" enabled="true">
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" 
          guiclass="HTTPArgumentsPanel" testclass="Arguments" 
          testname="Variables pré-définies" enabled="true">
            <collectionProp name="Arguments.arguments">
              <elementProp name="username" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">true</boolProp>
                <stringProp name="Argument.value">jrfitzsimmons</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">username</stringProp>
              </elementProp>
              <elementProp name="password" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">true</boolProp>
                <stringProp name="Argument.value">password</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">password</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.domain"></stringProp>
          <stringProp name="HTTPSampler.port"></stringProp>
          <stringProp name="HTTPSampler.connect_timeout"></stringProp>
          <stringProp name="HTTPSampler.response_timeout"></stringProp>
          <stringProp name="HTTPSampler.protocol"></stringProp>
          <stringProp name="HTTPSampler.contentEncoding"></stringProp>
          <stringProp name="HTTPSampler.path">/auth/login</stringProp>
          <stringProp name="HTTPSampler.method">POST</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">false</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.monitor">false</boolProp>
          <stringProp name="HTTPSampler.embedded_url_re"></stringProp>
        </HTTPSamplerProxy>
        <hashTree>
          <ResultCollector guiclass="StatVisualizer" testclass="ResultCollector" 
          testname="Aggregate Report" enabled="true">
            <boolProp name="ResultCollector.error_logging">false</boolProp>
            <objProp>
              <name>saveConfig</name>
              <value class="SampleSaveConfiguration">
                <time>true</time>
                <latency>true</latency>
                <timestamp>true</timestamp>
                <success>true</success>
                <label>true</label>
                <code>true</code>
                <message>true</message>
                <threadName>true</threadName>
                <dataType>true</dataType>
                <encoding>false</encoding>
                <assertions>true</assertions>
                <subresults>true</subresults>
                <responseData>false</responseData>
                <samplerData>false</samplerData>
                <xml>false</xml>
                <fieldNames>false</fieldNames>
                <responseHeaders>false</responseHeaders>
                <requestHeaders>false</requestHeaders>
                <responseDataOnError>false</responseDataOnError>
                <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
                <assertionsResultsToSave>0</assertionsResultsToSave>
                <bytes>true</bytes>
                <threadCounts>true</threadCounts>
              </value>
            </objProp>
            <stringProp name="filename"></stringProp>
          </ResultCollector>
          <hashTree/>
          <Summariser guiclass="SummariserGui" testclass="Summariser" 
          testname="Generate Summary Results" enabled="true"/>
          <hashTree/>
          <ResultCollector guiclass="GraphVisualizer" testclass="ResultCollector" 
          testname="Graph Results" enabled="true">
            <boolProp name="ResultCollector.error_logging">false</boolProp>
            <objProp>
              <name>saveConfig</name>
              <value class="SampleSaveConfiguration">
                <time>true</time>
                <latency>true</latency>
                <timestamp>true</timestamp>
                <success>true</success>
                <label>true</label>
                <code>true</code>
                <message>true</message>
                <threadName>true</threadName>
                <dataType>true</dataType>
                <encoding>false</encoding>
                <assertions>true</assertions>
                <subresults>true</subresults>
                <responseData>false</responseData>
                <samplerData>false</samplerData>
                <xml>false</xml>
                <fieldNames>false</fieldNames>
                <responseHeaders>false</responseHeaders>
                <requestHeaders>false</requestHeaders>
                <responseDataOnError>false</responseDataOnError>
                <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
                <assertionsResultsToSave>0</assertionsResultsToSave>
                <bytes>true</bytes>
                <threadCounts>true</threadCounts>
              </value>
            </objProp>
            <stringProp name="filename"></stringProp>
          </ResultCollector>
          <hashTree/>
        </hashTree>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" 
        testname="View Messages" enabled="true">
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" 
          guiclass="HTTPArgumentsPanel" testclass="Arguments" 
          testname="Variables pré-définies" enabled="true">
            <collectionProp name="Arguments.arguments"/>
          </elementProp>
          <stringProp name="HTTPSampler.domain"></stringProp>
          <stringProp name="HTTPSampler.port"></stringProp>
          <stringProp name="HTTPSampler.connect_timeout"></stringProp>
          <stringProp name="HTTPSampler.response_timeout"></stringProp>
          <stringProp name="HTTPSampler.protocol"></stringProp>
          <stringProp name="HTTPSampler.contentEncoding"></stringProp>
          <stringProp name="HTTPSampler.path">/api/rooms/1/messages</stringProp>
          <stringProp name="HTTPSampler.method">GET</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.monitor">false</boolProp>
          <stringProp name="HTTPSampler.embedded_url_re"></stringProp>
        </HTTPSamplerProxy>
        <hashTree>
          <ResultCollector guiclass="StatVisualizer" testclass="ResultCollector" 
          testname="Aggregate Report" enabled="true">
            <boolProp name="ResultCollector.error_logging">false</boolProp>
            <objProp>
              <name>saveConfig</name>
              <value class="SampleSaveConfiguration">
                <time>true</time>
                <latency>true</latency>
                <timestamp>true</timestamp>
                <success>true</success>
                <label>true</label>
                <code>true</code>
                <message>true</message>
                <threadName>true</threadName>
                <dataType>true</dataType>
                <encoding>false</encoding>
                <assertions>true</assertions>
                <subresults>true</subresults>
                <responseData>false</responseData>
                <samplerData>false</samplerData>
                <xml>false</xml>
                <fieldNames>false</fieldNames>
                <responseHeaders>false</responseHeaders>
                <requestHeaders>false</requestHeaders>
                <responseDataOnError>false</responseDataOnError>
                <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
                <assertionsResultsToSave>0</assertionsResultsToSave>
                <bytes>true</bytes>
                <threadCounts>true</threadCounts>
              </value>
            </objProp>
            <stringProp name="filename"></stringProp>
          </ResultCollector>
          <hashTree/>
        </hashTree>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" 
        testname="View Rooms" enabled="true">
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" 
          guiclass="HTTPArgumentsPanel" testclass="Arguments" 
          testname="Variables pré-définies" enabled="true">
            <collectionProp name="Arguments.arguments"/>
          </elementProp>
          <stringProp name="HTTPSampler.domain"></stringProp>
          <stringProp name="HTTPSampler.port"></stringProp>
          <stringProp name="HTTPSampler.connect_timeout"></stringProp>
          <stringProp name="HTTPSampler.response_timeout"></stringProp>
          <stringProp name="HTTPSampler.protocol"></stringProp>
          <stringProp name="HTTPSampler.contentEncoding"></stringProp>
          <stringProp name="HTTPSampler.path">/api/rooms</stringProp>
          <stringProp name="HTTPSampler.method">GET</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.monitor">false</boolProp>
          <stringProp name="HTTPSampler.embedded_url_re"></stringProp>
        </HTTPSamplerProxy>
        <hashTree>
          <ResultCollector guiclass="StatGraphVisualizer" testclass="ResultCollector"
           testname="Aggregate Graph" enabled="true">
            <boolProp name="ResultCollector.error_logging">false</boolProp>
            <objProp>
              <name>saveConfig</name>
              <value class="SampleSaveConfiguration">
                <time>true</time>
                <latency>true</latency>
                <timestamp>true</timestamp>
                <success>true</success>
                <label>true</label>
                <code>true</code>
                <message>true</message>
                <threadName>true</threadName>
                <dataType>true</dataType>
                <encoding>false</encoding>
                <assertions>true</assertions>
                <subresults>true</subresults>
                <responseData>false</responseData>
                <samplerData>false</samplerData>
                <xml>false</xml>
                <fieldNames>false</fieldNames>
                <responseHeaders>false</responseHeaders>
                <requestHeaders>false</requestHeaders>
                <responseDataOnError>false</responseDataOnError>
                <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
                <assertionsResultsToSave>0</assertionsResultsToSave>
                <bytes>true</bytes>
                <threadCounts>true</threadCounts>
              </value>
            </objProp>
            <stringProp name="filename"></stringProp>
          </ResultCollector>
          <hashTree/>
        </hashTree>
        <ResultCollector guiclass="GraphVisualizer" testclass="ResultCollector"
         testname="Graph Results" enabled="true">
          <boolProp name="ResultCollector.error_logging">false</boolProp>
          <objProp>
            <name>saveConfig</name>
            <value class="SampleSaveConfiguration">
              <time>true</time>
              <latency>true</latency>
              <timestamp>true</timestamp>
              <success>true</success>
              <label>true</label>
              <code>true</code>
              <message>true</message>
              <threadName>true</threadName>
              <dataType>true</dataType>
              <encoding>false</encoding>
              <assertions>true</assertions>
              <subresults>true</subresults>
              <responseData>false</responseData>
              <samplerData>false</samplerData>
              <xml>false</xml>
              <fieldNames>false</fieldNames>
              <responseHeaders>false</responseHeaders>
              <requestHeaders>false</requestHeaders>
              <responseDataOnError>false</responseDataOnError>
              <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
              <assertionsResultsToSave>0</assertionsResultsToSave>
              <bytes>true</bytes>
              <threadCounts>true</threadCounts>
              <sampleCount>true</sampleCount>
            </value>
          </objProp>
          <stringProp name="filename"></stringProp>
        </ResultCollector>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy"
         testname="Logout" enabled="true">
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" 
          guiclass="HTTPArgumentsPanel" testclass="Arguments" testname="Variables pré-définies"
           enabled="true">
            <collectionProp name="Arguments.arguments"/>
          </elementProp>
          <stringProp name="HTTPSampler.domain"></stringProp>
          <stringProp name="HTTPSampler.port"></stringProp>
          <stringProp name="HTTPSampler.connect_timeout"></stringProp>
          <stringProp name="HTTPSampler.response_timeout"></stringProp>
          <stringProp name="HTTPSampler.protocol"></stringProp>
          <stringProp name="HTTPSampler.contentEncoding"></stringProp>
          <stringProp name="HTTPSampler.path">/auth/logout</stringProp>
          <stringProp name="HTTPSampler.method">DELETE</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">false</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.monitor">false</boolProp>
          <stringProp name="HTTPSampler.embedded_url_re"></stringProp>
        </HTTPSamplerProxy>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" 
        testname="Login Invalid" enabled="true">
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" 
          guiclass="HTTPArgumentsPanel" testclass="Arguments"
           testname="Variables pré-définies" enabled="true">
            <collectionProp name="Arguments.arguments">
              <elementProp name="username" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value">asdasdasdasd</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">username</stringProp>
              </elementProp>
              <elementProp name="password" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value">asdasdasdlaksdjlasd</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">password</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.domain"></stringProp>
          <stringProp name="HTTPSampler.port"></stringProp>
          <stringProp name="HTTPSampler.connect_timeout"></stringProp>
          <stringProp name="HTTPSampler.response_timeout"></stringProp>
          <stringProp name="HTTPSampler.protocol"></stringProp>
          <stringProp name="HTTPSampler.contentEncoding"></stringProp>
          <stringProp name="HTTPSampler.path">/auth/login</stringProp>
          <stringProp name="HTTPSampler.method">POST</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">false</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.monitor">false</boolProp>
          <stringProp name="HTTPSampler.embedded_url_re"></stringProp>
        </HTTPSamplerProxy>
        <hashTree/>
        <CookieManager guiclass="CookiePanel" testclass="CookieManager" 
        testname="HTTP Cookie Manager" enabled="true">
          <collectionProp name="CookieManager.cookies"/>
          <boolProp name="CookieManager.clearEachIteration">true</boolProp>
        </CookieManager>
        <hashTree/>
        <ResultCollector guiclass="RespTimeGraphVisualizer" 
        testclass="ResultCollector" testname="Response Time Graph" 
        enabled="true">
          <boolProp name="ResultCollector.error_logging">false</boolProp>
          <objProp>
            <name>saveConfig</name>
            <value class="SampleSaveConfiguration">
              <time>true</time>
              <latency>true</latency>
              <timestamp>true</timestamp>
              <success>true</success>
              <label>true</label>
              <code>true</code>
              <message>true</message>
              <threadName>true</threadName>
              <dataType>true</dataType>
              <encoding>false</encoding>
              <assertions>true</assertions>
              <subresults>true</subresults>
              <responseData>false</responseData>
              <samplerData>false</samplerData>
              <xml>false</xml>
              <fieldNames>false</fieldNames>
              <responseHeaders>false</responseHeaders>
              <requestHeaders>false</requestHeaders>
              <responseDataOnError>false</responseDataOnError>
              <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
              <assertionsResultsToSave>0</assertionsResultsToSave>
              <bytes>true</bytes>
              <threadCounts>true</threadCounts>
            </value>
          </objProp>
          <stringProp name="filename"></stringProp>
          <stringProp name="RespTimeGraph.graphtitle">Average Response Times</stringProp>
          <stringProp name="RespTimeGraph.interval">500</stringProp>
        </ResultCollector>
        <hashTree/>
        <ResultCollector guiclass="StatGraphVisualizer" testclass="ResultCollector"
         testname="Aggregate Graph" enabled="true">
          <boolProp name="ResultCollector.error_logging">false</boolProp>
          <objProp>
            <name>saveConfig</name>
            <value class="SampleSaveConfiguration">
              <time>true</time>
              <latency>true</latency>
              <timestamp>true</timestamp>
              <success>true</success>
              <label>true</label>
              <code>true</code>
              <message>true</message>
              <threadName>true</threadName>
              <dataType>true</dataType>
              <encoding>false</encoding>
              <assertions>true</assertions>
              <subresults>true</subresults>
              <responseData>false</responseData>
              <samplerData>false</samplerData>
              <xml>false</xml>
              <fieldNames>false</fieldNames>
              <responseHeaders>false</responseHeaders>
              <requestHeaders>false</requestHeaders>
              <responseDataOnError>false</responseDataOnError>
              <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
              <assertionsResultsToSave>0</assertionsResultsToSave>
              <bytes>true</bytes>
              <threadCounts>true</threadCounts>
            </value>
          </objProp>
          <stringProp name="filename"></stringProp>
        </ResultCollector>
        <hashTree/>
        <AuthManager guiclass="AuthPanel" testclass="AuthManager"
         testname="HTTP Authorization Manager" enabled="true">
          <collectionProp name="AuthManager.auth_list"/>
        </AuthManager>
        <hashTree/>
        <Summariser guiclass="SummariserGui" testclass="Summariser"
         testname="Generate Summary Results" enabled="true"/>
        <hashTree/>
        <ResultCollector guiclass="SplineVisualizer" 
        testclass="ResultCollector" testname="Spline Visualizer"
         enabled="true">
          <boolProp name="ResultCollector.error_logging">false</boolProp>
          <objProp>
            <name>saveConfig</name>
            <value class="SampleSaveConfiguration">
              <time>true</time>
              <latency>true</latency>
              <timestamp>true</timestamp>
              <success>true</success>
              <label>true</label>
              <code>true</code>
              <message>true</message>
              <threadName>true</threadName>
              <dataType>true</dataType>
              <encoding>false</encoding>
              <assertions>true</assertions>
              <subresults>true</subresults>
              <responseData>false</responseData>
              <samplerData>false</samplerData>
              <xml>false</xml>
              <fieldNames>false</fieldNames>
              <responseHeaders>false</responseHeaders>
              <requestHeaders>false</requestHeaders>
              <responseDataOnError>false</responseDataOnError>
              <saveAssertionResultsFailureMessage>false</saveAssertionResultsFailureMessage>
              <assertionsResultsToSave>0</assertionsResultsToSave>
              <bytes>true</bytes>
              <threadCounts>true</threadCounts>
            </value>
          </objProp>
          <stringProp name="filename"></stringProp>
        </ResultCollector>
        <hashTree/>
      </hashTree>
    </hashTree>
  </hashTree>
</jmeterTestPlan>
```
