% SWEN90007 Software Design And Architecture - Project 2
% Software Architectural Design for Taut Messaging Feature
% Mathew Blair and James Fitzsimmons


Overview
========

This document details the high-level architecture for our messaging application, Taut. It contains: high-level domain, architecture, and component diagrams; diagrams specifying how the various parts of the system interact; and our reasoning for choosing particular patterns.


Team Members
============

+ Mathew Blair - 541635
+ James Fitzsimmons - 382043


High-Level Diagrams
===================
Here we provide the high level structural views of the Taut messaging application. These views demonstrate the relevant architectrual components needed to satisfy the messaging use case, and do not represent the entire architecture of the application. 

### Taut Domain
The domain of Taut, as shown in Figure \autoref{domain}, is that of a messaging system, and is independent of the particular implementation (that is a web based messaging system). Further, it represents the whole domain of this application, not just the messaging feature.

Notably, there is no concept of a `User` in the Taut domain. A `User` is a strictly Use-Case level entity, a way of restricting access to a resource based on some set of credentials. User accounts are also transient: a person may leave an organisation and therefore we may be required to remove their user account so that they no longer have access to the system. In these instances we will still want, for auditing purposes, to be able to track the actions and messages of certain individuals, of `People`. Whilst this distinction may not be necessary for a consumer focussed app (where we may not care about the person once they have left) it is prudent to include this distinction in our enterprise application for both accountability purposes and to allow us the flexibility to change how we handle User accounts without modifying the domain layer. 

![Domain diagram for Taut. \label{domain}](domain.png "Domain diagram for Taut.")

## Architectural Diagram

At a the component level Taut follows the principles of The Clean Architecture, relying on dependency inversion to separate the more fragile and changeable aspects of the application from the constant or rarely modified components. Because of this there is a heavy reliance on interfaces. Further, these interfaces are often defined in the components that rely on them. For example, the `Person` component will define the `IPersonRepo`, that will be implemented in the `Repository` sub-component of the `Interface Adapter Layer`. These interfaces, along with where they are defined, are given in Figure \autoref{component}.

![Component diagram for Taut. \label{component}](component.png "Component diagram for Taut.")

### Interface Specifications
Here we provide a brief description as to the purpose of each interface, along with the methods on them. Note that this is not a complete implementation of the Taut system, and that these interfaces will be extended as more functionality is added.

### Infrastructure Layer Interfaces
#### IGoCraft
The `IGoCraft` provides the all of the basic web framework functionality required by our application. It is provided to us by the GoCraft/web framework. For details of the methods provided by this framework, please see the documentation for the framework [here](https://github.com/gocraft/web).

#### IGORM
The `IGORM` provides the required functionality for the repositories to perform database actions. It is provided by the GORM ORM library, and relies on the definition of annotated types that are implemented in the Repositories component. For full details of this interface, please see the GORM documentation [here](https://github.com/jinzhu/gorm).

### Interface Adapter Layer Interfaces 
#### ITautMessagingAPI
The `Taut Messaging API	` is the public facing interface to the Taut Messaging system. This API is a restful JSON API, and is provided through resource oriented HTTP Requests. It provides the facility to create a message in a room and view a rooms messages, as well as delete and edit messages.

##### Methods
```
	// Return a list of the rooms available to the user.
	GET    "/api/rooms"

	// Delete a room.
	DELETE "/api/rooms/:id"

	// Gets the most recent messages in a room, up to some limit.
	GET    "/api/rooms/:room_id/messages?limit=10"

	// Post a new message in a room.
	POST   "/api/rooms/:room_id/messages"

	// Edit an existing message.
	POST   "/api/messages/:id"
```

#### IUserRepo
The `IUserRepo`, implemented in the Interface Adapter Layer, is defined in the User component of the Use Case Layer and specifies the required methods to retrieve and store objects in the Person repository.

##### Methods
```
	FindById(id : int) : (User, error)
```

#### IPersonRepo
The `IPersonRepo`, implemented in the Interface Adapter Layer, is defined in the Person component of the Domain Layer and specifies the required methods to retrieve and store objects in the Person repository.

##### Methods
```
	FindById(id : int) : (Person, error)
```

#### IMessageRepo
The `IMessageRepo`, implemented in the Interface Adapter Layer, is defined in the Message component of the Domain Layer and specifies the required methods to retrieve and store objects in the Message repository.

##### Methods
```
	FindById(id : int) (Message, error)
	FindByRoom(room : Room, limit : int) : ([]Message, error)
	Store(message : Message)
	Update(message : Message)
	Delete(message : Message)
```

#### IRoomRepo
The `IRoomRepo`, implemented in the Interface Adapter Layer, is defined in the Room component of the Domain Layer and specifies the required methods to retrieve and store objecfts in the Room repository.

##### Methods
```
	All() : ([]Room, error)
	FindById(id : int) : (Room, error)
	Delete(room : Room)
```

#### IMessageContext 
This private interface defines the relavent methods to handle all requests dealing with Messages in Taut. It is defined and implemented in the MessageContext component.

##### Methods
```
	MessageEdit(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	MessageDelete(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
```

#### IRoomContext
This private interface defines the relevant methods to handle all requests dealing with Rooms in Taut. It is defined and implemented in the RoomContext component.

##### Methods
```
	RoomsList(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomMessagesList(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomMessagePost(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
	RoomDelete(c : *ApiContext, rw : web.ResponseWriter, req : *web.Request)
```

### Use Case Layer Interfaces
#### IRoomInteractor
The IRoomInteractor interface provides the input and output interfaces for all Room manipulation Use Cases. It provides these interfaces through function definitions, that are implemented in the Room Interactor component.

##### Methods
```
	ListAll(presenter : IRoomPresenter, userId : int)
	AddMessage(presenter : IRoomPresenter, userId : int, roomId : int, content : string)
	LoadMessages(presenter : IRoomPresenter, userId : int, roomId : int, limit : int)
	Delete(presenter : IRoomPresenter, userId : int, roomId : int)
```

#### IMessageInteractor
The IMessageInteractor interface provides the input and output interfaces for all Room manipulation Use Cases. It provides these interfaces through function definitions, that are implemented in the Message Interactor component.

##### Methods
```
	Edit(presenter : IMessagePresenter, userId : int, messageId : int, newContent : string)
```

#### IUser
The private IUser interface exports the User usecase entity data type and associated interfaces to the Use Case layer for use within use cases. It may, in future, be exported publically to the Interface Adapter Layer for use in filters in the `TautFrontController`.

### Domain Layer Interfaces 
#### IDomain
The `IDomain` interface exports the data types and functions avaialable on the Domain models to the Use Case layer for use in implementing the Use Case, as well as the defining the associated interfaces.


Structural Views
================
Here we show a class diagram of the implementation and specification of the interfaces within Taut. This is shown on a per component basis for clarity. Given these interfaces are well specified, it is entirely appropraite to work on components in isolation to ensure clarity. The interfaces are as specified above, and the domain entities are specified in the domain model.

![Class diagram for Taut’s Domain Component.](domain_class.pdf "Class diagram for Taut’s Domain Component.")

![Class diagram for Taut’s Interface Adapter Component.](interface_adapters.pdf "Class diagram for Taut’s Interface Adapter Component.")

![Class diagram for Taut’s Interface Adapter Repositories Component.](interface_adapters_repos.pdf "Class diagram for Taut’s Interface Adapter Repositories Component.")

![Class diagram for Taut’s Use Case Component.](user_case_class.pdf "Class diagram for Taut’s Use Case Component.")


Behavioural Views
=================
These views demonstrate the behaviour of the system by demonstrating the sequence of events required to handle any and all relevant incoming requests. They make use of the interfaces defined earlier in the Interface Specifications to accomplish this.

Interaction Diagrams
--------------------
Here we specifiy the indvidual intercations required to satisfy these use cases. Where possible, interactions are defined earlier and reused in later components to clarify the documentation and reflect the desire for non-duplicated implementation.

### General Interactions
These interactions demonstrate the reusable components of future sequence diagrams, such as retrieveing a `Room` using the `RoomRepository`. They also define the sequence diagrams for handling the incoming requests and the request lifecycle, presented in generic terms.

#### Retrieving A Room
This sequence diagram shows how the retrieval of a room from the `RoomRepository` functions. 

![Retrieving A Room](retrieve_room.png "Retrieving A Room.")

#### Handling Requests
Here we present the method by which the contexts are retrieved and initialised in an incoming request. Because of the nature of GoLang, some of the interactions here are hard to represent (due to heavy use of annonymous functions) and as such have been reduced to their equivalent in `setUserID` and `setInteractors`.

In practice, this setup is handled by the GoCraft/web library. We present it here to provide an understanding as to how requests are being handled and provide insight as to where contexts come from. This diagram will be used in demonstrating how each individual request type works by retrieving the context.

![Request Handling](request_handling.png "Request Handling.")

#### Retrieving A Message
This sequence diagram shows the interaction required to retrieve a message from the `MessageRepository`.

![Retrieving A Message.](retrieve_message.png "Retrieving A Message.")

### Posting A Message
The interaction diagram in \autoref{interaction-msg-post} demonstrates the flow of events to post a new message. At the present time, given there has been no implementation for authentication; all users can post to any room. 

![Interaction diagram for Taut’s message posting use case. \label{interaction-msg-post}](post_message.pdf "Interaction diagram for Taut’s message posting use case.")

### Editting A Message
The interaction diagram in \autoref{interaction-msg-edit} demonstrates the behaviour required to edit an existing message. It simplifies the nature of parsing parameters to simplify behaviour (this actually occurs inline).

![Interaction diagram for Taut’s message editing use case. \label{interaction-msg-edit}](edit_message.pdf)
 
### Deleting A Room
The interaction diagram in \autoref{interaction-msg-delete} demonstrates the behaviour required to remove. At present, this is the only way to delete a group of messages or make them unavailable. The check is made for the room being owned by the user requesting the action and if they are not equal, the action is disallowed.

![Interaction diagram for Taut’s Room deleting use case. \label{interaction-msg-delete}](delete_room.pdf)

### Reading Messages in A Room 
The interaction diagram in \autoref{interaction-room-read} demonstrates the behaviour required to view all messages within a room. Once again, at this stage there is no authentication, nor access control implemented and so all users can access all rooms.

![Interaction diagram for Taut’s room message reading use case. \label{interaction-room-read}](read_room.pdf)


Architectural Patterns
======================

In designing the architecture for Taut, the decision was made to first understand the requirements of both the domain of the problem and the language that the application will be implemented. Whilst, in an ideal world, we would like the architectural decisions to be completely independent of the language of implementation, when we begin applying frameworks and language specific paradigms the choice of architecture can either greatly hinder or assist in the actual implementation effort and the eventual success of the architecture for the specific implementation. 

The architecture we have decided upon was borne out of research into architectural patterns and styles for GoLang. The architecture follows *The Clean Architecture* paradigm. The Clean Architecure is a specific variation of an n-tier (most often 4-tiered) layered architecture which places a very strong focus on the use of interfaces and dependency injection to allow for decoupling of the more changeable aspects of the application with the more steady domain and use case implementations. This idea, presented by Robert Martin (colloquially known as Uncle Bob from 8th light)[2], is an ideal choice for the programming style and structure of GoLang. 

The principles of The Clean Architecture are relatively simple: 

- All elements of some layer, n, define the interfaces that they need to fulfil their behaviour
- The layer above (n+1) then implements these interfaces and then injects the dependencies into the components in layer n, providing the implmentations required for that layer to function.

These two relatively simple ideas allow us to provide a significant level of felxibility but also structure to our application. This is often represented as a spherical layered structure, rather than a flat structure as most tiered architectures. This reflects the idea that the core of your application should be relatively unchanged, and change only with your domain or use cases. The more readily changeable aspects, database layers, interfaces, scalability efforts etc, are then on the *outside* of your application, and are free to change without having an impact on the use cases or domain of your applications. This is well represented in the diagram origianlly presented by Robert Martin, shown here in Figure \ref{clean_architecture}.

![The Clean Architecture, presented by Robert Martin [2]. \label{clean_architecture}](clean_architecture_diagram.jpg)

This structure allows us to implement our domain and use cases and have these completely independent of the interfaces, applications or databases they serve and use. Through the use of interfaces, we can specify that there must be some form of repository implemented in some higher level for the domain and provided this is implemented and conforms to this interface, the domain level will not have to change at all when we change databases or storage mechanisms. This would, for example, allow us to scale our application and add elements like in memory caching and multiple databases for different components (Postgres for relational objects, MongoDB for non relational documents or disk for files) and the domain will not need to be modified at all. 

Similarly, from an enteprise perspective this provides a considerable degree of flexibility in providing integration with existing systems. We could, for example, write an `ILoggerInterface` in the use case layer, which requires an implementation of this interface and will log all actions to this interface. We can then write an implementation of this interface that integrates into our existing audit trail software and we then have full functionality. The same can be said of database integration and authentication. We have, through dependency injection, the power to isolate entirely our use-cases and domain models from, as Robert Martin calls them, the *details* of the particular version of Taut. In doing this, our Use Case and Domain layers are also *free* from the constraints applied by frameworks that may be used to serve the interface or framework layers.

In this version of Taut we have made use of GORM and GoCraft as database and web frameworks to provide these services as it seems from our research that these are the best tools for this purpose at the moment. This concept is completely isolated from the use-case and domain layers - the domain simply relies on the repos and the use-cases rely on the input and output interfaces (i.e. the presenters) and are not aware of the fact that they are servicing a JSON API. This concept also makes it very straightforward for us to build other services that provide different interfaces to the Taut use-cases without having to rewrite or duplicate this code.

This dependency inversion also provides an easy facility for testing objects, as the concept of injecting 'mocked' interfaces for the purposes of testing is integrated into the architecture at its core, allowing us to write tests with injected fake depencies and not have to rely on extraneous mocking frameworks. 

In addition to the application of The Clean Architecture, the Interface Adapter layer includes an implementation of a Front Controller-like architecture, predominately by conforming to GoCraft/web framework requirements. In this manner, our routers act as front controllers, applying middleware to build a context and then calling the appropriate method to handle this request on the relevant context. 

Design Patterns
===============

Common Patterns
---------------

Throughout the application, Taut makes heavy use of the dependency injection pattern. Indeed, dependency injection is fundamental to the clean architecture pattern.

The dependency injection pattern allows each layer to specify functionality that it depends upon, without concerning itself with the actual implementation, by way of interfaces.

Not only does this provide a good separation of concerns, it facilitates proper unit testing, as classes can be easily tested with mocked dependencies. For large scale enterprise applications, adequate testability is an absolute must to ensure code is behaving as expected, and regressions are not introduced.

Data Persistence
----------------

For data persistence, Taut uses the repository pattern, with the domain layer specifying interfaces for each of the entities’ repositories. This provides an abstraction between the domain layer and the underlying storage technologies.

Underpinning the repository implementation is GORM, an object relational mapper for GoLang (as opposed to the Grails ORM with the same name). GORM handles mapping in-memory entity objects into rows in the database, and vice versa.

Although introducing a data mapper can add complexity to an application (by way of increased code), by delegating this to an existing library, the costs have been minimised, while maintaining the benefits of loose coupling between the domain and data storage layers.

A consequence of using the clean architecture pattern is that domain entities are unaware of implementation details of the data persistence. The nature of GORM is that is requires annotations on structs in order to set up relationships between entities. As such, Taut’s repository implementations handle translating between the database entities and the domain entities.

Wrapping a data mapper inside the repository pattern allows Taut’s domain layer to remain independent of persistence mechanism. If desired, the particular ORM library being used could be substituted, or even replaced with another mechanism such as a web API, all without changes to code in the domain layer.

Object-to-Relational Structure
------------------------------

GORM uses a number of design patterns to translate database rows into the in-memory entities.

As is very often the case when dealing with this problem, GORM uses the identity field pattern to track which struct corresponds to which database row. GORM can be configured to use meaningful or meaningless keys, but in Taut, only meaningless keys are used. The keys are all simple and table-unique, and are generated automatically by the database.

Relationships between entities are handled using both the foreign key mapping and association table mapping patterns. As GORM relies on the database to generate new keys for objects, sometimes inserting objects with relations requires two queries (so that the foreign key is known for the dependent object), however GORM handles this automatically.

One of the downsides to foreign key mapping is that it introduces coupling between the domain layer and the data persistence layer, as the domain entities must be aware of how the data layer tracks relationships. With the relationship pattern and its additional translation between domain entity structs and database entity structs, this downside is completely negated.

Object-to-Relational Behaviour
------------------------------

The identity map pattern could easily be used for Taut’s messaging feature, to cache recently posted messages in memory. One limitation with GORM though is that it doesn’t provide any facilities for caching objects. Should performance prove to be an issue, the relevant repositories’ implementations could easily be extended to make use of this pattern.

At this stage, Taut is not making use of the unit of work pattern. With the server being a stateless web API, each endpoint has been designed to perform a single focused task. Message posting can be handled by a single insert query, with updates, loads, and deletion also each only requiring a single query.

It should be noted that for the second feature, authorisation will have to be considered (the groups and ACLs), the logic for the message endpoints will become more complicated. With that, the unit of work pattern become more valuable.

Concurrency
-----------

With the messaging feature being reasonably simple, the common problems brought by introducing concurrency are not present.

The only operation at risk of interference is editing a message - if a user has two instances of the client application open, they could edit a message in one, and edit it differently in the second, before the second receives the notification that the original message has changed. However, as a message can only be edited by the user who posted it, Taut instead just updates the message contents to the last received change.

Again, for the second feature however, the increased complexity in logic will bring about possible concurrency issues, and at that point various locking patterns will have to be considered.

Presentation
------------

GoCraft/web is the web library on which Taut’s API layer is built. GoCraft/web provides a light wrapper around GoLang’s net/http library, uses the front controller design pattern for delegating request handling. Such a pattern allows for simple configuration, as all API requests can be passed through the same set of filters before being handed off to specialised functions.

In the clean architecture, the use case layer is responsible for determining what data needs to be shown. It then calls a presenter to show that data, with the presenter being defined in the interface adapter layer. In Taut, the presenter implementations transform the domain objects into JSON DAOs using the json-binding package.

Other
-----

This document is only concerned with the prototype implementation of Taut’s messaging feature. As such, sessions and security have been deemed as out of scope.

Performance will be evaluated once a prototype has been built, however as long as concurrency has been properly addressed, performance could be readily increased by running multiple instances of the Taut server application across a cluster of nodes, backed by a shared database instance.


References
==========
1. Novak J. gocraft/web. 2015. Available at: https://github.com/gocraft/web. Accessed September 8, 2015.
2. Martin R. The Clean Architecture | 8th Light. Blog8thlightcom. 2012. Available at: http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html. Accessed September 10, 2015.
3. Kiessling M. Applying The Clean Architecture to Go applications » The Log Book of Manuel Kiessling. Manuelkiesslingnet. 2012. Available at: http://manuel.kiessling.net/2012/09/28/applying-the-clean-architecture-to-go-applications/. Accessed September 10, 2015.
