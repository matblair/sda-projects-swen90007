# SWEN90007 Software Design And Architecture
## Project One


Overview
========

This document outlines the broader specification for the enterprise system that
we will be implementing sections for throughout the projects for SDA this
semester.


Team Members
============

+ Mathew Blair - 541635
+ James Fitzsimmons - 382043


Description Of System
=====================

The system we will be building is a deployable self-hosted software platform for
team communication and collaboration with strict access control protocols,
integration APIs (eg: for payroll systems or configuration management systems)
and inbuilt auditing and analytics for this communication. We call it `Taut`.

`Taut` is a platform that is designed to be used across business divisions
(called 'spaces') and allow compartmentalised discussion within project groups
('rooms').

This system is intended to replace email within businesses and therefore also
allows file uploads, groups for communication (the equivalent of email threads),
multiple interfaces (mobile, web, and API for integration) and will be
distributed in nature to run across multiple machines, in multiple locations
serving eventually consistent data across multiple clients simultaneously.

Given its enterprise use, all communication will also be encrypted and tools
like Two-Factor Authentication will be imposed for access to the system.

Messaging Features
------------------

The core service offered by the application is messaging. Users will be able to
message others users directly, or message rooms or teams that they are involved
in (provided it is allowed by the access control protocol). Within these
messages they will be able to:

- Send basic text messages
- Send longer form, more formal announcements to groups (called 'posts')
- Send files
- Comment on sent files
- Edit previously sent messages (if allowed by access control protocol and
  within certain time limits and view constraints)

These features will be available across all previously mentioned interfaces.

User Accounts
-------------

Users will be given accounts that they can use to log in, with integrations
provided if companies wish to integrate into their own authentication system.
These user accounts can be one of two categories:

- **Staff**: a standard user governed by access control protocols.
- **Administrator**: a super user with the power to create and remove groups.
  These users can also create and remove spaces, and assign users to the access
  control group for administering spaces.

Access Control
--------------

Access Control Groups form the basis for restricting who can and cannot access
certain teams, rooms and spaces. The access control protocol for a space cannot
be overruled by any room in that space.

Access Rights are hierarchical where each level implies the rights of the previous
level. The levels are as follows:

- Visibility (can see the room but not read its contents)
- Read Access (can see and read the room's messages and files)
- Write Access (can post in the room)
- Admin Access (can add or remove people from the room, change access control
  rights)

The access control groups themselves are also hierarchical, with the following
requirements:

- Groups can inherit from multiple groups.
- Groups inherit the rights of their parent groups.
- The inheritance must be acyclic.

Access rights are assigned to individual groups in an access control list, where
an administrator of a room or space can determine what access (visibility, read,
write or admin) the group has to that room or space.

To determine a particular access right (eg: visibility, or write) for a user to
a particular room or space we use the following procedure:

- For each group that the user is in, and all parent groups of those groups, up
  to the top of the tree, check whether that group has been either allowed or
  denied access to the room.
- If no groups have been found that allow access for the right (or higher), then
  deny the user.
- If any groups have been found that deny access for the right (or lower), then
  deny the user.
- Otherwise, allow the user.

Integration API
---------------

The system will aim to provide an API that allows programmatic access to the
functionalities described above. That is, the creation, reading and writing of
messages and rooms. API access will be provided through API keys, which can be
created only by users with an Administration role.

Auditing
--------

As this is an enterprise system, it is crucial that there is a strong logging,
auditing and analytics component for both legal and performance management uses.

Therefore, this platform will record all actions in an audit log that will
identity the action, the resource this action occurred on, the time, and the user
who took the action.


Use Cases for Messaging Feature
===============================

Reading Messages
----------------

### Trigger

The user wishes to join a room to read messages.

### Preconditions

- The user is an employee at the company.
- The user is signed in.

### Basic Path

1. The user searches the list of rooms and attempts to join the room they're
   interested in.
2. The server verifies that the user has read access to the room.
3. The user granted access to the room.
4. The user is presented with the most recent messages in the room.

### Postconditions

None.

### Exception Paths

- At step 1, the search results will only show rooms that the user has
  visibility rights. If the room is not visible to the user, then the use case
  ends.
- At step 2, if the user doesn't have read access, then an error message is
  shown to the user and the use case ends.

Posting a Message
-----------------

### Trigger

The user wishes to post a message in a room.

### Preconditions

- The user is an employee at the company.
- The user is signed in.
- The user is in the room they wish to post in.

### Basic Path

1. The user types and submits their message.
2. The server verifies that the user has permission to post in the room.
3. Other members in the room are notified of the message.

### Postconditions

- The message is persisted in the room's message history.

### Exception Paths

- At step 2, if the user does not have permission to post in the room, then the
  message is not saved, and the use case ends.

Editing a Message
-----------------

### Trigger

The user wishes to edit a message they have sent.

### Preconditions

- The user is an employee at the company.
- The user is signed in.
- The user is in the room they wish to post in.
- The user has posted a message in the room.

### Basic Path

1. The user edits their message.
2. The server verifies that the message being edited belongs to the user, is the
   most recent message in the room, and that it has been less than one minute
   since the message was originally posted.
3. Other members in the room are notified of the changed message.

### Postconditions

- The room's message history is updated with the new message contents.

### Exception Paths

- At step 2, if any of the conditions fail, then the message is not edited, and
  the use case ends.

Deleting Messages
-----------------

### Trigger

The user wishes to delete a room.

### Preconditions

- The user is an employee at the company.
- The user is signed in.

### Basic Path

1. The user attempts to delete the room.
2. The server verifies that user has admin rights for the room being deleted.
3. The room is deleted.

### Postconditions

- The messages that had been posted in the room are deleted.

### Exception Paths

- At step 2, if the user doesn't have read access, then an error message is
  shown to the user and the use case ends.
- At step 3, if the server has been configured to not allow message deletion,
  then the room is archived (same appearance to end users but the data is kept)
  and the use case ends.
