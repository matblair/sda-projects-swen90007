// user_repository.go

/*
 This file contains the implementation for the IUserRepository specified in the
 use case layer.
 */

package repositories

import (
	"bitbucket.org/matblair/sda-projects-swen90007/taut/usecases"
	"github.com/jinzhu/gorm"
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
)

type UserRepository struct {
	DB gorm.DB
}


/*
 Utility functions.
 */

func (repository *UserRepository) toDomain(gormUser GormUser) usecases.User {
	// Find the linked person entity.
	gormPerson := GormPerson{}
	repository.DB.Model(&gormUser).Related(&gormPerson, "PersonId")

	return usecases.User {
		Id: gormUser.Id,
		Username: gormUser.Username,
		HashedPassword: gormUser.HashedPassword,
		Admin: gormUser.Admin,
		Person: domain.Person{
			Id: gormPerson.Id,
			Name: gormPerson.Name,
			Alias: gormPerson.Alias,
		},
	}
}


/*
 Interface implementation.
 */

func (repository *UserRepository) FindById(id int) (usecases.User, error) {
	// Load the database entity.
	gormUser := GormUser{}
	query := repository.DB.First(&gormUser, id)

	// Return an error if the query failed.
	if query.Error != nil {
		return usecases.User{}, query.Error
	}

	// Covert to domain model.
	return repository.toDomain(gormUser), nil
}

func (repository *UserRepository) FindByUsername(username string) (usecases.User, error) {
	// Load the database entity.
	gormUser := GormUser{}
	query := repository.DB.
		Where("Username = ?", username).
		First(&gormUser)

	// Return an error if the query failed.
	if query.Error != nil {
		return usecases.User{}, query.Error
	}

	// Covert to domain model.
	return repository.toDomain(gormUser), nil
}
