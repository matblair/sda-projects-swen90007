// room_repository.go

/*
 This file contains the implementation for the IRoomRepository specified in the
 domain layer.
 */

package repositories

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
)

type RoomRepository struct {
	DB gorm.DB
}


/*
 Utility functions.
 */

func (repository *RoomRepository) toDomain(gormRoom GormRoom) domain.Room {
	// Load the owner of the room.
	gormPerson := GormPerson{}
	repository.DB.Model(&gormRoom).Related(&gormPerson, "OwnerId")

	return domain.Room{
		Id: gormRoom.Id,
		Name: gormRoom.Name,
		Owner: domain.Person{
			Id: gormPerson.Id,
			Name: gormPerson.Name,
			Alias: gormPerson.Alias,
		},
		People: nil,
	}
}

func (repository *RoomRepository) toGorm(room domain.Room) GormRoom {
	return GormRoom{
		Id: room.Id,
		Name: room.Name,
		OwnerId: room.Owner.Id,
	}
}


/*
 Interface implementation.
 */

func (repository *RoomRepository) All() ([]domain.Room, error) {
	// Load the database entities.
	gormRooms := []GormRoom{}
	query := repository.DB.Find(&gormRooms)

	// Return an error if the query failed.
	if query.Error != nil {
		return []domain.Room{}, query.Error
	}

	// Covert them to domain models.
	rooms := make([]domain.Room, len(gormRooms))
	for idx, gormRoom := range gormRooms {
		rooms[idx] = repository.toDomain(gormRoom)
	}

	return rooms, nil
}

func (repository *RoomRepository) FindById(id int) (domain.Room, error) {
	// Load the database entity.
	gormRoom := GormRoom{}
	query := repository.DB.First(&gormRoom, id)

	// Return an error if the query failed.
	if query.Error != nil {
		return domain.Room{}, query.Error
	}

	// Covert to domain model.
	return repository.toDomain(gormRoom), nil
}

func (repository *RoomRepository) Delete(room domain.Room) {
	// The model has a deleted_at field, so this is a soft delete.
	gormRoom := repository.toGorm(room)
	repository.DB.Delete(&gormRoom)
}