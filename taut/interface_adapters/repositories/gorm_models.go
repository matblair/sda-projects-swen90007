// gorm_models.go

/*
 This file contains structs for GORM database tables. Although very similar
 to the domain models, they've been redefined here as GORM requires specific
 annotations.
*/

package repositories

import "time"

type GormMessage struct {
	Id         int

	Content    string `sql:"type:text"`

	PostedAt   time.Time

	Edited     bool

	DeletedAt  time.Time         // soft delete

	PostedBy   GormPerson
	PostedById int               // foreign key

	Room       GormRoom
	RoomId     int `sql:"index"` // foreign key
}

type GormRoom struct {
	Id        int

	Name      string

	DeletedAt time.Time // soft delete

	Owner     GormPerson
	OwnerId   int       // foreign key

	People    []GormPerson `gorm:"many2many:room_people;"`

	Messages  []GormMessage
}

type GormPerson struct {
	Id    int

	Name  string

	Alias string

	Rooms []GormRoom `gorm:"many2many:room_people;"`
}

type GormUser struct {
	Id             int

	Username       string `sql:"unique"`

	HashedPassword string

	Admin          bool

	Person         GormPerson
	PersonId       int // foreign key
}
