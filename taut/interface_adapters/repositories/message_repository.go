// message_repository.go

/*
 This file contains the implementation for the IMessageRepository specified in
 the domain layer.
 */

package repositories

import (
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
	"github.com/jinzhu/gorm"
	"strings"
)

type MessageRepository struct {
	DB gorm.DB
}


/*
 Utility functions.
 */

func (repository *MessageRepository) toDomain(gormMessage GormMessage) domain.Message {
	// Load the person who posted the message.
	gormPerson := GormPerson{}
	repository.DB.Model(&gormMessage).Related(&gormPerson, "PostedById")

	// Load the room the message was posted in.
	gormRoom := GormRoom{}
	repository.DB.Model(&gormMessage).Related(&gormRoom, "RoomId")

	return domain.Message{
		Id: gormMessage.Id,
		Content: gormMessage.Content,
		PostedAt: gormMessage.PostedAt,
		Edited: gormMessage.Edited,
		Room: domain.Room{
			Id: gormRoom.Id,
			Name: gormRoom.Name,
		},
		PostedBy: domain.Person {
			Id: gormPerson.Id,
			Name: gormPerson.Name,
			Alias: gormPerson.Alias,
		},
	}
}

func (repository *MessageRepository) mapToDomain(gormMessages []GormMessage) []domain.Message {
	// Covert them to domain models.
	messages := make([]domain.Message, len(gormMessages))
	for idx, gormMessage := range gormMessages {
		messages[idx] = repository.toDomain(gormMessage)
	}
	return messages
}

func (repository *MessageRepository) toGorm(message domain.Message) GormMessage {
	return GormMessage{
		Id: message.Id,
		Content: message.Content,
		PostedAt: message.PostedAt,
		Edited: message.Edited,
		PostedById: message.PostedBy.Id,
		RoomId: message.Room.Id,
	}
}


/*
 Interface implementation.
 */

func (repository *MessageRepository) FindById(id int) (domain.Message, error) {
	// Load the database entity.
	gormMessage := GormMessage{}
	query := repository.DB.First(&gormMessage, id)

	// Return nil if not found.
	if query.Error != nil {
		return domain.Message{}, query.Error
	}

	// Covert to domain model.
	return repository.toDomain(gormMessage), nil
}

func (repository *MessageRepository) FindByIds(ids []int) ([]domain.Message, error) {
	// Load the database entities.
	gormMessages := []GormMessage{}
	query := repository.DB.
		Where(ids).
		Order("posted_at").
		Find(&gormMessages)

	// Return an error if the query failed.
	if query.Error != nil {
		return []domain.Message{}, query.Error
	}

	// Covert to domain model.
	return repository.mapToDomain(gormMessages), nil
}

func (repository *MessageRepository) FindByRoom(room domain.Room, limit int) ([]domain.Message, error) {
	// Load the database entities.
	gormMessages := []GormMessage{}
	query := repository.DB.
		Where("room_id = ?", room.Id).
		Order("posted_at").
		Limit(limit).
		Find(&gormMessages)

	// Return an error if the query failed.
	if query.Error != nil {
		return []domain.Message{}, query.Error
	}

	// Covert them to domain models.
	messages := make([]domain.Message, len(gormMessages))
	for idx, gormMessage := range gormMessages {
		messages[idx] = repository.toDomain(gormMessage)
	}

	return messages, nil
}

func (repository *MessageRepository) FindByKeywords(room domain.Room, keywords []string) ([]domain.Message, error) {
	// Load the database entities.
	gormMessages := []GormMessage{}

	// Create the keyword search string.
	keywordMatcher := "Content LIKE ?"
	keywordMatcher = keywordMatcher + strings.Repeat(" OR " + keywordMatcher, len(keywords) - 1)

	// Mmmm, Go's type system...
	vals := make([]interface{}, len(keywords))
	for i, v := range keywords {
		vals[i] = "%" + v + "%"
	}

	query := repository.DB.
		Where("room_id = ?", room.Id).
		Where(keywordMatcher, vals...).
		Order("posted_at").
		Find(&gormMessages)

	// Return an error if the query failed.
	if query.Error != nil {
		return []domain.Message{}, query.Error
	}

	// Covert them to domain models.
	messages := make([]domain.Message, len(gormMessages))
	for idx, gormMessage := range gormMessages {
		messages[idx] = repository.toDomain(gormMessage)
	}

	return messages, nil
}

func (repository *MessageRepository) Store(message domain.Message) {
	// Create the database entity model.
	gormMessage := repository.toGorm(message)

	// Save to the database.
	repository.DB.Create(&gormMessage)
}

func (repository *MessageRepository) Update(message domain.Message) {
	// Create the database entity model.
	gormMessage := repository.toGorm(message)

	// Update the database row.
	repository.DB.Save(&gormMessage)
}

func (repository *MessageRepository) Delete(message domain.Message) {
	// The model has a deleted_at field, so this is a soft delete.
	gormMessage := repository.toGorm(message)
	repository.DB.Delete(&gormMessage)
}

func (repository *MessageRepository) DeleteAll(messages []domain.Message) {
	// We have to do this in a transaction because Go's default ValueConverter
	// does not support `[]int` types.
	tx := repository.DB.Begin()
	for _, message := range messages {
		gormMessage := repository.toGorm(message)
		tx.Delete(&gormMessage)
	}
	tx.Commit()
}

