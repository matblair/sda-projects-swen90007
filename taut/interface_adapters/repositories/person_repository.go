// person_repository.go

/*
 This file contains the implementation for the IPersonRepository specified in
 the domain layer.
 */

package repositories

import (
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
	"github.com/jinzhu/gorm"
)

type PersonRepository struct {
	DB gorm.DB
}


/*
 Utility functions.
 */

func (repository *PersonRepository) toDomain(gormPerson GormPerson) domain.Person {
	return domain.Person{
		Id: gormPerson.Id,
		Name: gormPerson.Name,
		Alias: gormPerson.Alias,
	}
}


/*
 Interface implementation.
 */

func (repository *PersonRepository) FindById(id int) (domain.Person, error) {
	// Load the database entity.
	gormPerson := GormPerson{}
	query := repository.DB.First(&gormPerson, id)

	// Return nil if not found.
	if query.Error != nil {
		return domain.Person{}, query.Error
	}

	// Covert to domain model.
	return repository.toDomain(gormPerson), nil
}
