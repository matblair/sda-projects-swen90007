// rooms_context.go

/*
 This file contains context definitions for handling web requests for rooms,
 allowing the client to access the room use cases.
*/

package web_services

import (
	"github.com/gocraft/web"
	"github.com/corneldamian/json-binding"
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
	"strconv"
	"strings"
)

/*
 Request structs.
 */

type MessageCreate struct {
	MessageContent string
}


/*
 Context functions to handle requests.
 */

func (c *ApiContext) RoomsList(rw web.ResponseWriter, req *web.Request) {
	presenter := JsonRoomPresenter{ Context: c }
	c.RoomInteractor.ListAll(&presenter, c.CurrentUserId)
}

func (c *ApiContext) RoomMessagesList(rw web.ResponseWriter, req *web.Request) {
	roomId, _ := strconv.Atoi(req.PathParams["room_id"])
	presenter := JsonRoomPresenter{ Context: c }
	c.RoomInteractor.LoadMessages(&presenter, c.CurrentUserId, roomId, 10)
}

func (c *ApiContext) RoomSearchMessages(rw web.ResponseWriter, req *web.Request) {
	roomId, _ := strconv.Atoi(req.PathParams["room_id"])
	keywords := strings.Split(req.FormValue("keywords"), ",")
	presenter := JsonRoomPresenter{ Context: c }
	c.RoomInteractor.SearchMessages(&presenter, c.CurrentUserId, roomId, keywords)
}

func (c *ApiContext) RoomCreateNewMessage(rw web.ResponseWriter, req *web.Request) {
	roomId, _ := strconv.Atoi(req.PathParams["room_id"])
	messageContent := req.FormValue("message_content")
	presenter := JsonRoomPresenter{ Context: c }
	c.RoomInteractor.AddMessage(&presenter, c.CurrentUserId, roomId, messageContent)
}

func (c *ApiContext) RoomMergeMessages(rw web.ResponseWriter, req *web.Request) {
	roomId, _ := strconv.Atoi(req.PathParams["room_id"])

	messageIdStrs := strings.Split(req.FormValue("message_ids"), ",")
	messagesIds := make([]int, len(messageIdStrs))
	// For some reason the Go compiler complains if messageIdStrs is used here,
	// because it thinks the array is `[]int` type. So yes, this is bad, but
	// it's required.
	for idx, messageIdStr := range strings.Split(req.FormValue("message_ids"), ",") {
		messagesIds[idx], _ = strconv.Atoi(messageIdStr)
	}

	presenter := JsonRoomPresenter{ Context: c }
	c.RoomInteractor.MergeMessages(&presenter, c.CurrentUserId, roomId, messagesIds)
}

func (c *ApiContext) RoomDelete(rw web.ResponseWriter, req *web.Request) {
	roomId, _ := strconv.Atoi(req.PathParams["room_id"])
	presenter := JsonRoomPresenter{ Context: c }
	c.RoomInteractor.Delete(&presenter, c.CurrentUserId, roomId)
}


/*
 JSON presenter for all the context functions to use.
 */

type JsonRoomPresenter struct {
	Context *ApiContext
}

func (presenter *JsonRoomPresenter) PresentListAll(rooms []domain.Room, err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse(&rooms)
	}
}

func (presenter *JsonRoomPresenter) PresentSearchMessages(messages []domain.Message, err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse(&messages)
	}
}

func (presenter *JsonRoomPresenter) PresentAddMessage(message domain.Message, err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse(&message)
	}
}

func (presenter *JsonRoomPresenter) PresentLoadMessages(messages []domain.Message, err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse(&messages)
	}
}

func (presenter *JsonRoomPresenter) PresentMergeMessages(message domain.Message, err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse(&message)
	}
}

func (presenter *JsonRoomPresenter) PresentDelete(err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse("Success.")
	}
}
