// authentication_context.go

/*
 This file contains context definitions for handling web requests for
 authentication, allowing the client to login and logout.
*/

package web_services

import (
	"github.com/gocraft/web"
	"bitbucket.org/matblair/sda-projects-swen90007/taut/usecases"
	"net/http"
)

/*
 Context functions to handle requests.
 */

func (c *Context) AuthLogin(rw web.ResponseWriter, req *web.Request) {
	username := req.Header.Get("username")
	password := req.Header.Get("password")
	presenter := JsonAuthenticationPresenter{ Context: c, rw: rw, req: req }
	c.UserInteractor.ValidateCredentials(&presenter, username, password)
}

func (c *Context) AuthLogout(rw web.ResponseWriter, req *web.Request) {
	session := c.Session
	delete(session.Values, "current_user_id")

	if err := session.Save(req.Request, rw); err != nil {
		http.Error(rw, err.Error(), 500)
		return
	}

	// Right okay
	rw.WriteHeader(http.StatusNoContent)
}

/*
 JSON presenter for all the context functions to use.
 */

type JsonAuthenticationPresenter struct {
	Context *Context
	rw web.ResponseWriter
	req *web.Request
}

func (presenter *JsonAuthenticationPresenter) PresentValidateCredentials(user usecases.User, err error) {
	if err != nil {
		http.Error(presenter.rw, "Authentication failed.", http.StatusUnauthorized)
		return
	}

	session := presenter.Context.Session
	session.Values["current_user_id"] = user.Id

	if err := session.Save(presenter.req.Request, presenter.rw); err != nil {
		http.Error(presenter.rw, err.Error(), 500)
		return
	}

	presenter.rw.WriteHeader(http.StatusOK)
	presenter.rw.Write([]byte("Credentials accepted.\n"))
}
