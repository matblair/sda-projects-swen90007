// messages_context.go

/*
 This file contains context definitions for handling web requests for messages,
 allowing the client to access the message use cases.
*/

package web_services

import (
	"bitbucket.org/matblair/sda-projects-swen90007/domain"
	"github.com/corneldamian/json-binding"
	"github.com/gocraft/web"
	"strconv"
)

/*
 Request structs.
 */

type MessageUpdate struct {
	MessageContent string
}


/*
 Context functions to handle requests.
 */

func (c *ApiContext) MessageUpdate(rw web.ResponseWriter, req *web.Request) {
	messageId, _ := strconv.Atoi(req.PathParams["message_id"])
	newContent := req.FormValue("message_content")
	presenter := JsonMessagePresenter{ Context: c }
	c.MessageInteractor.Edit(&presenter, c.CurrentUserId, messageId, newContent)
}


/*
 JSON presenter for all the context functions to use.
 */

type JsonMessagePresenter struct {
	Context *ApiContext
}

func (presenter *JsonMessagePresenter) PresentEdit(message domain.Message, err error) {
	if err != nil {
		presenter.Context.ResponseJSON = binding.ErrorResponse(err.Error(), 500)
	} else {
		presenter.Context.ResponseJSON = binding.SuccessResponse(&message)
	}
}
