// contexts.go

// This file defines the default context structs for the web API functions. As
// the API is authenticated, it contains a reference to the currently logged in
// user. All contexts contain references to the various interactors.

package web_services

import (
	"bitbucket.org/matblair/sda-projects-swen90007/taut/usecases"
	"github.com/gorilla/sessions"
)

type Context struct {
	MessageInteractor usecases.IMessageInteractor
	RoomInteractor    usecases.IRoomInteractor
	UserInteractor    usecases.IUserInteractor
	Session           *sessions.Session
}

type ApiContext struct {
	*Context
	CurrentUserId int
	ResponseJSON  interface{}
}