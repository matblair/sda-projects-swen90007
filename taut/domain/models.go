// models.go

/*
 This file contains the domain models for Taut.
 */

package domain

import "time"

type Message struct {
	Id             int
	Content        string
	Edited         bool
	PostedAt       time.Time
	Room           Room
	PostedBy       Person
}

type Room struct {
	Id       int
	Name     string
	Owner    Person
	People   []Person
}

type Person struct {
	Id    int
	Name  string
	Alias string
}
