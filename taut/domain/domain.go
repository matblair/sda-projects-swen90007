// domain.go

/*
 The file contains interface definitions for the various repository structs.
 These interfaces are then used by outer layers.
 */

package domain

type IRoomRepository interface {
	// Returns a list of all of the rooms. Rooms will have owner person structs
	// loaded, but People will be nil.
	All() ([]Room, error)

	// Returns the room corresponding to the given ID, or an error if none
	// exists. Room will have owner person struct loaded, but People will be
	// nil.
	FindById(id int) (Room, error)

	// Deletes or otherwise hides a room.
	Delete(room Room)
}

type IMessageRepository interface {
	// Returns the message corresponding to the given ID, or an error if none
	// exists. Message will have person and room structs loaded, but those
	// structs will have nil for their other relationships.
	FindById(id int) (Message, error)

	// Returns the message corresponding to the given IDs, or an error if
	// something goes wrong. Messages will have person and room structs loaded,
	// but those structs will have nil for their other relationships.
	FindByIds(id []int) ([]Message, error)

	// Returns a list of the most recent messages in a room, up to `limit`
	// messages. Messages will have person and room structs loaded, but those
	// structs will have nil for their other relationships.
	FindByRoom(room Room, limit int) ([]Message, error)

	// Returns a list of messages that match a list of keywords, within a
	// particular room. Messages will have person and room structs loaded, but
	// those structs will have nil for their other relationships.
	FindByKeywords(room Room, keywords []string) ([]Message, error)

	// Saves a new message in the repository.
	Store(message Message)

	// Updates an existing message in the repository.
	Update(message Message)

	// Deletes or otherwise hides a message.
	Delete(message Message)

	// Deletes or otherwise hides a list of messages.
	DeleteAll(messages []Message)
}

type IPersonRepository interface {
	// Returns the person corresponding to the given ID, or an error if none
	// exists.
	FindById(id int) (Person, error)
}
