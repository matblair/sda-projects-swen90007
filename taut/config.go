package main

import (
	"log"
	"os"

	"gopkg.in/gcfg.v1"
)

var configuration Configuration

type Configuration struct {
	SqlLite struct {
		Path string
	}
}

func init() {
	err := gcfg.ReadFileInto(&configuration, "config.gcfg")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}