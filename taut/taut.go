package main

import (
	"log"
	"net/http"
	"os"

	. "bitbucket.org/matblair/sda-projects-swen90007/taut/usecases"
	. "bitbucket.org/matblair/sda-projects-swen90007/taut/interface_adapters/web_services"
	. "bitbucket.org/matblair/sda-projects-swen90007/taut/interface_adapters/repositories"

	"github.com/gocraft/web"
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"github.com/corneldamian/json-binding"
	"time"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
	"fmt"
)

type InteractorMiddleware struct {
	MessageInteractor IMessageInteractor
	RoomInteractor IRoomInteractor
	UserInteractor IUserInteractor
}

func (im *InteractorMiddleware) CreateFilter() func(*Context, web.ResponseWriter, *web.Request, web.NextMiddlewareFunc) {
	return func (ctx *Context, rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
		ctx.MessageInteractor = im.MessageInteractor
		ctx.RoomInteractor = im.RoomInteractor
		ctx.UserInteractor = im.UserInteractor
		next(rw, r)
	}
}

func SessionMiddleware(ctx *Context, rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	session, err := cookieStore.Get(r.Request, "user")
	if err != nil {
		http.Error(rw, err.Error(), 500)
		return
	}
	ctx.Session = session
	next(rw, r)
}


/*
 Authorise the user.
 */

var cookieStore = sessions.NewCookieStore([]byte("something-very-secret"))

func ApiAuthMiddleware(ctx *ApiContext, rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	currentUserId, _ := ctx.Session.Values["current_user_id"].(int)

	// Default value for int is 0.
	if currentUserId > 0 {
		ctx.CurrentUserId = currentUserId
		next(rw, r)
	} else {
		http.Error(rw, "Not Authenticated.", http.StatusUnauthorized)
	}
}


/*
 DB seed method.
 */

func Seed(db gorm.DB) {
	james := GormPerson{
		Name: "James",
		Alias: "jf",
	}
	mat := GormPerson{
		Name: "Mat",
		Alias: "mb",
	}

	db.Create(&james)
	db.Create(&mat)

	jamesPassword, _ := bcrypt.GenerateFromPassword([]byte("password"), bcrypt.DefaultCost)
	matPassword, _ := bcrypt.GenerateFromPassword([]byte("password"), bcrypt.DefaultCost)

	jamesUser := GormUser{
		Username: "jrfitzsimmons",
		HashedPassword: string(jamesPassword),
		Admin: true,
		PersonId: james.Id,
	}
	matUser := GormUser{
		Username: "matblair",
		HashedPassword: string(matPassword),
		Admin: false,
		PersonId: mat.Id,
	}

	db.Create(&jamesUser)
	db.Create(&matUser)

	room := GormRoom{
		Name: "Lobby",
		OwnerId: james.Id,
	}

	db.Create(&room)

	db.Model(&room).Association("People").Append(&james, &mat)

	jamesMsg := GormMessage{
		Content: "Hello, from James.",
		PostedAt: time.Now().Add(-time.Minute),
		Edited: false,
		PostedById: james.Id,
		RoomId: room.Id,
	}
	matMsg := GormMessage{
		Content: "Hello, from Mat.",
		PostedAt: time.Now(),
		Edited: false,
		PostedById: mat.Id,
		RoomId: room.Id,
	}

	db.Create(&jamesMsg)
	db.Create(&matMsg)
}

func main() {
	// Initialise the database.
	db, err := gorm.Open("sqlite3", "/tmp/taut.db")
	if err != nil {
		log.Fatal(err)
		os.Exit(2)
	}

	// Apply pending migrations.
	db.LogMode(true)
	db.DropTable(&GormUser{}, &GormPerson{}, &GormRoom{}, &GormMessage{})
	db.Exec("DROP TABLE room_people;")
	db.AutoMigrate(&GormUser{}, &GormPerson{}, &GormRoom{}, &GormMessage{})
	Seed(db)

	// Configure the cookie store.
	cookieStore.Options = &sessions.Options{
		Domain:   "localhost",
		Path:     "/",
		MaxAge:   3600 * 8,
		HttpOnly: true,
		Secure:   false,
	}

	// Set up the repositories.
	userRepo := UserRepository{ DB: db }
	personRepo := PersonRepository{ DB: db }
	roomRepo := RoomRepository{ DB: db }
	messageRepo := MessageRepository{ DB: db }

	// Set up the interactors.
	roomInteractor := RoomInteractor{
		UserRepository: &userRepo,
		PersonRepository: &personRepo,
		RoomRepository: &roomRepo,
		MessageRepository: &messageRepo,
	}
	messageInteractor := MessageInteractor{
		UserRepository: &userRepo,
		PersonRepository: &personRepo,
		MessageRepository: &messageRepo,
	}
	userInteractor := UserInteractor{
		UserRepository: &userRepo,
	}

	// Middleware objects.
	interactorFilterProvider := InteractorMiddleware{
		MessageInteractor: &messageInteractor,
		RoomInteractor: &roomInteractor,
		UserInteractor: &userInteractor,
	}

	// Create the messaging router.
	router := web.New(Context{})
	router.Middleware(web.LoggerMiddleware)
	router.Middleware(web.ShowErrorsMiddleware)
	router.Middleware(interactorFilterProvider.CreateFilter())
	router.Middleware(SessionMiddleware)
	// User lotgin and logout
	router.Post("/auth/login", (* Context).AuthLogin)
	router.Delete("/auth/logout", (* Context).AuthLogout)

	apiRouter := router.Subrouter(ApiContext{}, "/api")
	apiRouter.Middleware(binding.Response(nil))
	apiRouter.Middleware(ApiAuthMiddleware)
	apiRouter.Get("/rooms", (* ApiContext).RoomsList)
	apiRouter.Get("/rooms/:room_id/messages", (* ApiContext).RoomMessagesList)
	apiRouter.Get("/rooms/:room_id/messages/search", (* ApiContext).RoomSearchMessages)
	apiRouter.Post("/rooms/:room_id/messages", (* ApiContext).RoomCreateNewMessage)
	apiRouter.Delete("/rooms/:room_id", (* ApiContext).RoomDelete)
	apiRouter.Post("/rooms/:room_id/messages/merge", (* ApiContext).RoomMergeMessages)
	apiRouter.Patch("/message/:message_id", (* ApiContext).MessageUpdate)

	// Listen.
	fmt.Fprintf(os.Stdout, "Server started.\n")
	http.ListenAndServe("localhost:3000", router)
}
