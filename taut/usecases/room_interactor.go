// room_interactor.go

/*
 This file contains logic for the messaging use cases. It includes interactors
 for the web API layer to use to achieve the required functionality of the
 application. It also defines an output interface, to be implemented in the
 outer web layer.
*/

package usecases

import (
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
	"time"
	"fmt"
	"bytes"
	"os"
)

/*
 * The interfaces that are required for the use cases.
 */

type IRoomPresenter interface {
	PresentListAll([]domain.Room, error)
	PresentAddMessage(domain.Message, error)
	PresentLoadMessages([]domain.Message, error)
	PresentSearchMessages([]domain.Message, error)
	PresentMergeMessages(domain.Message, error)
	PresentDelete(error)
}

type IRoomInteractor interface {
	// Lists all of the rooms which the user is allowed to see.
	ListAll(presenter IRoomPresenter, userId int)

	// Adds a message to a room's message history, if the user is allowed to
	// post in the room.
	AddMessage(presenter IRoomPresenter, userId int, roomId int, content string)

	// Loads the most recent messages from a room's message history, if the
	// user is allowed to read messages in the room.
	LoadMessages(presenter IRoomPresenter, userId int, roomId int, limit int)

	// Searches for the messages from a room's message history, matching some
	// list of keywords.
	SearchMessages(presenter IRoomPresenter, userId int, roomId int, keywords []string)

	// Merge two messages into a new message, if the user is the room owner and
	// both the messages are from the room.
	MergeMessages(presenter IRoomPresenter, userId int, roomId int, messages []int)

	// Deletes a room, if the user has administrator privileges.
	Delete(presenter IRoomPresenter, userId int, roomId int)
}


/*
 Utility functions.
 */

func (interactor *RoomInteractor) allMessagesInRoom(room domain.Room, messages []domain.Message) bool {
	for _, message := range messages {
		if message.Room.Id != room.Id {
			return false
		}
	}
	return true
}


/*
 * Implementation of the use case interfaces.
 */

type RoomInteractor struct {
	RoomRepository    domain.IRoomRepository
	MessageRepository domain.IMessageRepository
	PersonRepository  domain.IPersonRepository
	UserRepository    IUserRepository
}

func (interactor *RoomInteractor) ListAll(presenter IRoomPresenter, userId int) {
	// For the moment, the user is allowed to see all rooms.
	rooms, err := interactor.RoomRepository.All()

	// Error if the repository returned an error.
	if err != nil {
		presenter.PresentListAll([]domain.Room{}, err)
		return
	}

	presenter.PresentListAll(rooms, nil)
}

func (interactor *RoomInteractor) AddMessage(presenter IRoomPresenter, userId int, roomId int, content string) {
	// Load the entities from the repository.
	room, _ := interactor.RoomRepository.FindById(roomId)
	user, _ := interactor.UserRepository.FindById(userId)

	// For the moment, the user is allowed to post in any room.

	// Create the message entity.
	message := domain.Message{
		Content: content,
		PostedBy: user.Person,
		PostedAt: time.Now(),
		Edited: false,
		Room: room,
	}

	// Save the message.
	interactor.MessageRepository.Store(message)

	presenter.PresentAddMessage(message, nil)
}

func (interactor *RoomInteractor) LoadMessages(presenter IRoomPresenter, userId int, roomId int, limit int) {
	// Load the entities from the repository.
	room, _ := interactor.RoomRepository.FindById(roomId)
	messages, _ := interactor.MessageRepository.FindByRoom(room, limit)

	// For the moment, the user is allowed to see messages in all rooms.

	presenter.PresentLoadMessages(messages, nil)
}

func (interactor *RoomInteractor) SearchMessages(presenter IRoomPresenter, userId int, roomId int, keywords []string) {
	// Load the entities from the repository.
	room, _ := interactor.RoomRepository.FindById(roomId)
	messages, err := interactor.MessageRepository.FindByKeywords(room, keywords)

	// Error if the repository returned an error.
	if err != nil {
		presenter.PresentSearchMessages([]domain.Message{}, err)
		return
	}

	presenter.PresentSearchMessages(messages, nil)
}

func (interactor *RoomInteractor) MergeMessages(presenter IRoomPresenter, userId int, roomId int, messageIds []int) {
	// Load the entities from the repository.
	room, _ := interactor.RoomRepository.FindById(roomId)
	messages, _ := interactor.MessageRepository.FindByIds(messageIds)
	user, _ := interactor.UserRepository.FindById(userId)
	person := user.Person

	// Error if the messages are not from the room.
	if !interactor.allMessagesInRoom(room, messages) {
		message := "Messages do not all belong to room #%i."
		err := fmt.Errorf(message, roomId)
		presenter.PresentMergeMessages(domain.Message{}, err)
		return
	}

	// Error if the counts are inconsisten.
	if len(messages) != len(messageIds) {
		message := "Not all the messages could be found."
		err := fmt.Errorf(message, roomId)
		presenter.PresentMergeMessages(domain.Message{}, err)
		return
	}

	// Error if the person trying to merge the messages is not the room owner.
	fmt.Fprintf(os.Stderr, "ids %v %v\n", person.Id, room.Owner.Id)
	if person.Id != room.Owner.Id {
		message := "Person #%i must be the owner of room #%i to merge messages."
		err := fmt.Errorf(message, person.Id, roomId)
		presenter.PresentMergeMessages(domain.Message{}, err)
		return
	}

	// Proceed with the merging.
	var buffer bytes.Buffer
	buffer.WriteString("Merged Messages")
	for _, message := range messages {
		format := "\n\nAt %s, %s wrote:\n%s"
		part := fmt.Sprintf(format, message.PostedAt.String(), message.PostedBy.Name, message.Content)
		buffer.WriteString(part)
	}

	// Create the message entity.
	message := domain.Message{
		Content: buffer.String(),
		PostedBy: person,
		PostedAt: time.Now(),
		Edited: false,
		Room: room,
	}

	// Save the message, delete the merged messages.
	interactor.MessageRepository.Store(message)
	interactor.MessageRepository.DeleteAll(messages)

	presenter.PresentMergeMessages(message, nil)
}

func (interactor *RoomInteractor) Delete(presenter IRoomPresenter, userId int, roomId int) {
	// Load the entities from the repository.
	room, _ := interactor.RoomRepository.FindById(roomId)
	user, _ := interactor.UserRepository.FindById(userId)
	person := user.Person

	// Error if the room is not owned by the person.
	if person.Id != room.Owner.Id {
		message := "Person #%i is not allowed to delete room #%i."
		err := fmt.Errorf(message, person.Id, roomId)
		presenter.PresentDelete(err)
		return
	}

	// Delete the room.
	interactor.RoomRepository.Delete(room)

	presenter.PresentDelete(nil)
}
