// user_interactor.go

/*
 This file contains logic for the authentication use cases. It includes
 interactors for the web API layer to use to achieve the required functionality
 of the application. It also defines an output interface, to be implemented in
 the outer web layer.
*/

package usecases

import "golang.org/x/crypto/bcrypt"

/*
 * The interfaces that are required for the use cases.
 */

type IUserPresenter interface {
	PresentValidateCredentials(User, error)
}

type IUserInteractor interface {
	// Validate's a user's username and password, returning the user if the
	// credentials match, and an error otherwise.
	ValidateCredentials(presenter IUserPresenter, username string, password string)
}


/*
 * Implementation of the use case interfaces.
 */

type UserInteractor struct {
	UserRepository IUserRepository
}

func (interactor *UserInteractor) ValidateCredentials(presenter IUserPresenter, username string, password string) {
	user, err := interactor.UserRepository.FindByUsername(username)

	if err != nil {
		presenter.PresentValidateCredentials(User{}, err)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.HashedPassword), []byte(password))

	if err != nil {
		presenter.PresentValidateCredentials(User{}, err)
		return
	}

	presenter.PresentValidateCredentials(user, nil)
}