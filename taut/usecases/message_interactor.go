// message_interactor.go

/*
 This file contains logic for the messaging use cases. It includes interactors
 for the web API layer to use to achieve the required functionality of the
 application. It also defines an output interface, to be implemented in the
 outer web layer.
*/

package usecases

import (
	"time"
	"bitbucket.org/matblair/sda-projects-swen90007/taut/domain"
	"fmt"
)


/*
 * The interfaces that are required for the use cases.
 */

type IMessagePresenter interface {
	PresentEdit(domain.Message, error)
}

type IMessageInteractor interface {
	// Updates the contents of a message entity, if the user attempting to edit
	// the message is the person who posted it originally, and that the time
	// since it was originally posted is less than one minute.
	Edit(presenter IMessagePresenter, userId int, messageId int, newContent string)
}


/*
 * Implementation of the use case interfaces.
 */

type MessageInteractor struct {
	MessageRepository domain.IMessageRepository
	PersonRepository  domain.IPersonRepository
	UserRepository    IUserRepository
}

func (interactor *MessageInteractor) Edit(presenter IMessagePresenter, userId int, messageId int, newContent string) {
	// Store the current time.
	now := time.Now()

	// Load the entities from the repository.
	user, _ := interactor.UserRepository.FindById(userId)
	person := user.Person
	message, err := interactor.MessageRepository.FindById(messageId)

	// Error if the repository returned an error.
	if err != nil {
		presenter.PresentEdit(message, err)
		return
	}

	// Error if the person was not the person who posted the message.
	if message.PostedBy.Id != person.Id {
		message := "Person #%i is not allowed to edit message #%i."
		err := fmt.Errorf(message, person.Id, messageId)
		presenter.PresentEdit(domain.Message{}, err)
		return
	}

	// Error if the time that message was posted is more than a minute ago.
	minute_ago := now.Add(-time.Minute)
	if minute_ago.After(message.PostedAt) {
		message := "Person #%i is no longer allowed to edit message #%i."
		err := fmt.Errorf(message, person.Id, messageId)
		presenter.PresentEdit(domain.Message{}, err)
		return
	}

	// Update the contents of the message.
	message.Content = newContent
	interactor.MessageRepository.Update(message)

	presenter.PresentEdit(message, nil)
}
