// user_model.go

/*
 This file contains the user model for Taut, as well as the repository
 interface to use to interact with users.
 */

package usecases

import "bitbucket.org/matblair/sda-projects-swen90007/taut/domain"

type User struct {
	Id             int
	Username       string
	HashedPassword string
	Admin          bool
	Person         domain.Person
}

type IUserRepository interface {
	// Returns the user corresponding to the given ID, or an error if none
	// exists. Person struct will be loaded, but Person's relations will be
	// nil.
	FindById(id int) (User, error)

	// Returns the user corresponding to the given username, or an error if
	// none exists. Person struct will be loaded, but Person's relations will
	// be nil.
	FindByUsername(username string) (User, error)
}
